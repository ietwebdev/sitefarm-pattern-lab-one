// eslint-disable-next-line import/no-extraneous-dependencies
import { defineConfig } from 'vite';

export default defineConfig(({ mode }) => {
  // Default config.
  const config = {
    // Use Pattern Lab's public directory as the root after Pattern Lab has run.
    root: 'public',

    // Vite assumes the "public" directory to be static assets. For development
    // we want this to be the dynamic root that we set above.
    publicDir: false,

    resolve: {
      alias: {
        // Fix patternlab using relative url that Vite cannot resolve.
        'styleguide': './styleguide'
      }
    },

    css: {
      preprocessorOptions: {
        scss: {
          // Use the modern SCSS compiler.
          api: 'modern',
          // Suppress deprecation warnings.
          // quietDeps: true,
        },
      },
    },

    // Need to change the default port since Docksal will listen to the default
    // 1573 port and cause patternlab to fail.
    server: {
      port: 1583,
    }
  };

  // If creating a production build or previewing it.
  if (mode === 'production') {
    config.root = './';
    config.publicDir = 'public';
    config.build = {
      outDir: 'build',
      rollupOptions: {
        // Overwrite default .html entry.
        input: [
          './source/sass/style.scss',
          './source/sass/vendor.scss',
          './source/sass/print.scss',
          './source/js/main.js',
        ],
        // Remove the [hash] so we can find the file with pattern lab.
        output: {
          entryFileNames: `assets/[name].js`,
          chunkFileNames: `assets/chunks/[name].[hash].js`,
          assetFileNames: `assets/[name].[ext]`,
        },
      },
    };
  }

  return config;
});
