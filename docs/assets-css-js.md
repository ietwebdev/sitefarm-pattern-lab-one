# Assets (CSS & JS)

To add either CSS or JS from third party packages to Pattern Lab, use one of
these methods:

**More Info:**

* CSS: https://github.com/ucdavis/ucd-theme-tasks/blob/master/docs/css.md
* JS: https://github.com/ucdavis/ucd-theme-tasks/blob/master/docs/javascript.md


## NPM Dependencies

NPM is a Node package manager for the web. It is useful for adding third party
libraries for both development and site inclusion.

Install any [NPM](https://www.npmjs.com/) component with:

```bash
$ npm install {package_name}
```

Use this when a package needs to be added as a dependency to the browser such as
using [Lit](https://www.npmjs.com/package/lit) to make web components.


### NPM SASS Dev Dependencies

Install any [NPM](https://www.npmjs.com/) component with the `--save-dev` flag
when only needed for development. This is helpful for Sass packages.

```bash
$ npm install {package_name} --save-dev
```

The following Sass libraries have been added for ease in development:

* [breakpoint-sass](http://breakpoint-sass.com/) - Media Query helper
* [normalize-scss](https://github.com/JohnAlbin/normalize-scss) - Normalize CSS
  reset
* [sass-burger](http://joren.co/sass-burger/) - Hambuger Menu

The JS in NPM Dependencies *will not* automatically be compiled and added to
production javascript.

[Vite](https://vitejs.dev/) is used to compile all NPM packages into
[Javascript ES Modules (ESM)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Modules)
that the browser can read. Adding a Javascript package into your code is as easy
as using an `import` in one of your JavaScript files.

```js
import package from 'package_name';
```


## NPM CSS Dependencies

CSS from npm packages can be added anywhere inside `.scss` files, but a special
`source/sass/_vendor.scss` file has been created for this purpose.

The `/node_modules/` path is automatically scanned so the import can start from
the package name.

```scss
@use "slick-carousel/slick/slick";
```


## NPM Javascript Dependencies

The JS in package.json `dependencies` *will not* automatically be compiled and added to production javascript.

See [Using Javascript](javascript.md) for information on how to require a package.


## Editing the head or foot partial

If you want to add custom scripts or tags into the header or footer then you can
use these files:

- `/source/_meta/head.hbs`
- `/source/_meta/foot.hbs`
