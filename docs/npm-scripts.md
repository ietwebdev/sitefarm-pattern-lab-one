# NPM Scripts

The `package.json` file has a handful of scripts that allow you to do a lot of
stuff within your development workflow like compile sass and update on changes.

## Default Tasks

There are 3 main scripts you should be aware of. Just add `npm run` before
each task like `$ npm run build`.

1. **start** - Generate the theme assets and start watching for changes to live
reload in the browser.
2. **build** - Generate the theme with all assets such as css and js.
3. **lint** - Validate CSS and JS by linting.

`$ npm start` is the one most often used and is a shorthand for `npm run start`

### UCD Theme Tasks

The [UCD Theme Tasks](https://www.npmjs.com/package/ucd-theme-tasks) node
package is a CLI tool. You likely will not need to use it directly but it is
good to be aware that the NPM scripts are using it.

If you would like to use it directly then use the `npx` command to use the
locally installed version with your theme.

```
$ npx ucd-theme-tasks --help
```

## Vite

[Vite](https://vitejs.dev/) is used to compile all code and can be
configured with the `vite.config.js` file.
https://vitejs.dev/config/

## Tasks Config
A `tasks-config.js` file provides extra configuration needed by some script
tasks like syncing pattern lab into a separate CMS theme. More information on
these settings can be found at
https://github.com/ucdavis/ucd-theme-tasks#default-tasks
