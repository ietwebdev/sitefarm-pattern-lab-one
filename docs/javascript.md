# Using Javascript

## Folder Structure

```
|-- js
|   |-- components/
|   |-- lib/
|   |-- main.js
``` 

### Components
Add standalone Web Components which could be built with tools like
[Lit](https://lit.dev/).

### Lib
Custom library files like helpers or other reusable modules.

### Files
**main.js**: This is the entry point for all javascript and contains an
[ESM](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Modules)
import of the needed js files.

Please use multiple individual module files and then import them in this file.


## Dependencies

See the [Assets (CSS & JS)](assets-css-js.md) documentation for how to add
javascript via NPM.


### Requiring packages

The JS in package.json `dependencies` *will not* automatically be compiled and
added to production javascript.

[Vite](https://vitejs.dev/) is used to compile all Javascript. Adding
a Javascript package into your code is as easy as using an
[import](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/import).

```js
import name from 'module-name';
```

To allow your own files to be imported by other files, be sure to
[export](https://developer.mozilla.org/en-US/docs/web/javascript/reference/statements/export)
code doing something like:

```js
// /source/js/add-stuff.js
export default function addStuff(a, b) {
  return a + b;
}
```

Now that function can be imported in the `main.js` file.

```js
// /source/js/main.js
import addStuff from './js/add-stuff.js';

console.log(addStuff(2, 3)); // print 5.
```

**More Info:**
https://github.com/ucdavis/ucd-theme-tasks/blob/master/docs/javascript.md


## Validation

[EsLint](https://eslint.org/) is used for JavaScript validation.

```bash
npm run lint:js
```

or try to automatically fix issues with:

```bash
npm run lint:js-fix
```


## Useful Articles and Links

* [Drual JS Coding Standards](https://www.drupal.org/node/172169)
* [jQuery Coding Standards](https://www.drupal.org/node/1720586)
