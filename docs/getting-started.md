# Getting Started

## Prerequisites

You'll need [node.js](http://nodejs.org).


## Install and start watching for changes

After cloning and changing into that directory, run this to install
dependencies:

```
$ npm ci
```

You may have to run that again for updates; so it may be wise to save this: `npm ci`. **If you have any problems; this is the first thing to run.**

To do an initial build of the site and start watching for changes while doing
local development run:

```
$ npm start
```

Finally, to bundle everything for production run:

```
$ npm run build
```

Look inside the `package.json` file under `scripts` and you will see several
scripts that can be executed by doing `npm run name-of-script`. These are used
for compiling code, running tests, and validating format.

## Windows Users

If you are on Windows you may run into a few issues.

It is recommended you use [WSL2 - Windows Subsystem for Linux](https://docs.microsoft.com/en-us/windows/wsl/install-win10).
