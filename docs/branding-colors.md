# Branding Colors

## Category Brand Colors

Components can be branded with a category color.

The following CSS variables can be used to automatically set colors to switch
to a brand color or contrast text on a branded background.

* `--category-brand` - Hex color for the brand (Ex. `#000`).
* `--category-brand-rgb` - RGB color for the brand (Ex. `0, 0, 0`).
* `--category-brand-rgb--dark` - A 50% darker shade of the RGB color (Ex. `0, 0, 0`).
* `--category-brand-contrast-color` - Hex contrasting color to the brand.
* `--category-brand-contrast-color-rgb` - RGB contrasting color to the brand (Ex. `0, 0, 0`).
* `--category-brand-filter` - Brightness filter to force contrast.
* `--category-brand-featured` - Hex color when a component is "featured".
* `--category-brand--on-white` - Hex color of branded text on a white background.

## Layout Section Colors

Layout Sections can have background colors and background images. This means
that any component in the layout needs to have automatic contrasts set up to
force a colors that will ensure contrasts against the background.

The following CSS variables can be used to automatically set a color that will
work with a Layout Section's background.

* `--forced-contrast` - Hex color to contrast a Layout Section background color (Ex. `#000`).
* `--forced-contrast-heading-primary` - Hex color for a Primary Heading to contrast a Layout Section background color.
* `--forced-contrast-heading-secondary` - Hex color for a Secondary Heading to contrast a Layout Section background color.
* `--forced-scheme` - Hex color for a light or dark Layout Section background.
* `--forced-scheme-contrast` - Hex color to contrast a light or dark scheme for a Layout Section background.

## CSS Color Cascade

There are two scenarios where colors in a component need to dynamically change:

1. Within a Layout Section that has a background color/image.
2. With a Category Brand color applied.

### Within a Layout Section that has a background color/image

Use a `--forced-***` variable and then fallback to the default color. 

```scss
color: var(--forced-contrast, #{$ucd-blue});
```

### With a Category Brand color applied

Category Brands usually need to also interact with Layout Section colors. This
means that there needs to be 3 layers of colors.

1. brand > default
2. brand > section > default
3. section > brand > default

#### Examples

Text on a branded background.
```scss
color: var(--category-brand-contrast-color, #{$ucd-blue});
```

If a brand color should win, then fall back to a layout section contrast, and
finally revert to the default color.
```scss
// Text to contrast to a branded background.
color: var(--category-brand-contrast-color, var(--forced-contrast, #{$ucd-blue}));
// Branded icon.
color: var(--category-brand, var(--forced-contrast, #{$ucd-gold}));
```

If a forced section color should win, then fall back to the brand color, and
finally revert to the default color.
```scss
// Button background color contrasting to the section. 
background-color: var(--forced-scheme, var(--category-brand, #{$ucd-gold}));
color: var(--forced-scheme-contrast, var(--category-brand-contrast-color, #{$ucd-blue}));
```

Branded Text on a White Background.
```scss
color: var(--forced-contrast, var(--category-brand--on-white, #{$ucd-blue}));
```

