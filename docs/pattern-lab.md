# About Pattern Lab

Pattern Lab is a frontend workshop environment that helps you build, view, test,
and showcase your design system's UI components.

- [Pattern Lab Website](https://patternlab.io/)
- [Documentation](https://patternlab.io/docs/editing-pattern-lab-source-files/)
- [Patterns](https://patternlab.io/docs/overview-of-patterns/)
- [Demos](https://patternlab.io/demos/)


## Handlebars Templates

Patterns in this version are using the Handlebars templating system.

https://handlebarsjs.com/
