# Table of Contents

* [Getting started](getting-started.md)
* [Assets (CSS & JS)](assets-css-js.md)
* [Export and sync styles into a New Site](export-sync.md)
* [NPM Scripts and Commands](npm-scripts.md)
* [Sass/Scss folder structure](sass-structure.md)
* [Branding Colors](branding-colors.md)
* [Using Javascript](javascript.md)
* [Tests](testing.md)
* [About Pattern Lab](pattern-lab.md)
* [Troublshooting](troubleshooting.md)
