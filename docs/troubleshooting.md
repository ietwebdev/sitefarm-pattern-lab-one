# Troubleshooting

## If you are getting errors

1. `nvm install`
2. delete all generated files and folders
  * node_modules
  * public
  * build
  * dependencyGraph.json
3. `npm ci`
4. `npm start`

