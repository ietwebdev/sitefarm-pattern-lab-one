export default {
  themeSync: {
    enabled: false,
    newsite: false,
    dest: '../../acquia/docroot/profiles/sitefarm/themes/sitefarm_one/',
    sassSync: true,
    jsSync: true,
    imagesSync: true,
    fontSync: true,
  },
  patternLab: {
    enabled: true
  }
}
