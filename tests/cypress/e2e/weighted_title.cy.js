describe('Weighted Title', () => {
  beforeEach(() => {
    cy.visit('/patterns/molecules-blocks-00-panel/molecules-blocks-00-panel.rendered.html')
  })

  it('The first word should be wrapped in a weighted class', () => {
    cy.get('.panel__title--weighted').should('have.text', 'Panel')
  })
})
