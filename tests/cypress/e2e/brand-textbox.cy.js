describe('Brand Textbox Collapse', () => {

  beforeEach(() => {
    cy.visit('/patterns/molecules-components-20-brand-textbox-collapse/molecules-components-20-brand-textbox-collapse.rendered.html')
  })

  it('Click the button to toggle the textbox area open and closed', () => {
    cy.get('[data-cy=content]').should('be.visible')
    cy.get('[data-cy=button]').click()
    cy.get('[data-cy=content]').should('not.be.visible')
    cy.get('[data-cy=button]').click()
    cy.get('[data-cy=content]').should('be.visible')
  })

  it('Accessibility attributes have been added and function on click', () => {
    cy.get('[data-cy=button]').should('have.attr', 'tabindex', '0')
    cy.get('[data-cy=button]').should('have.attr', 'aria-controls', 'msg-area-0')
    cy.get('[data-cy=content]').should('have.attr', 'id', 'msg-area-0')
    cy.get('[data-cy=button]').should('have.attr', 'aria-expanded', 'true')
    cy.get('[data-cy=button]').click()
    cy.get('[data-cy=button]').should('have.attr', 'aria-expanded', 'false')
    cy.get('[data-cy=button]').click()
    cy.get('[data-cy=button]').should('have.attr', 'aria-expanded', 'true')
  })

  it('Test session variables keeping message state after reload', () => {
    cy.get('[data-cy=content]').should('be.visible')
    cy.get('[data-cy=button]').click()
    cy.get('[data-cy=content]').should('not.be.visible')
    cy.reload()
    cy.get('[data-cy=content]').should('not.be.visible')
  })
})
