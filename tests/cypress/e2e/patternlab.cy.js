describe('Pattern Lab', () => {
  it('Pattern Lab loads', () => {
    cy.visit('/')

    cy.get('.pl-c-logo').should('contain', 'Pattern Lab')
  })
})
