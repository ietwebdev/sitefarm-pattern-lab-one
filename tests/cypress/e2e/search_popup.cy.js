describe('Search Popup', () => {
  const form = '.search-popup'

  beforeEach(() => {
    cy.visit('/patterns/molecules-components-12-search-popup/molecules-components-12-search-popup.rendered.html')
  })

  context('Responsive Change', () => {
    it('Form changes based on screen size', () => {
      cy.get(form).should('be.hidden')
      cy.viewport(320, 600)
      cy.get(form).should('be.visible')
      cy.viewport(1000, 660)
      cy.get(form).should('be.hidden')
    })
  })

  context('Desktop', () => {
    it('Search Popup toggle should be visible', () => {
      cy.get('.search-popup__open').should('be.visible')
    })

    it('Open and close the form', () => {
      cy.get(form).should('be.hidden')
      cy.get('.search-popup__open').click()
      cy.get(form).should('be.visible')
      cy.get('.search-popup__open--close').click()
      cy.get(form).should('be.hidden')
    })

    it('Close after popup opened and then focus backwards off of search popup button to close', () => {
      cy.visit('/patterns/templates-00-homepage/templates-00-homepage.rendered.html');
      cy.get(form).should('be.hidden')
      cy.get('.search-popup__open').click()
      cy.get(form).should('be.visible')
      cy.get('.search-popup__open').focus()
      cy.get('.site-branding__site-name a').focus()
      cy.get(form).should('be.hidden')
    })

    it('Close after popup opened and then focus continues forward out of the popup', () => {
      cy.visit('/patterns/templates-00-homepage/templates-00-homepage.rendered.html');
      cy.get(form).should('be.hidden')
      cy.get('.search-popup__open').click()
      cy.get(form).should('be.visible')
      cy.get('.search-form__submit').focus()
      cy.get('.quick-links__title').focus()
      cy.get(form).should('be.hidden')
    })

    it('Make sure the search popup has its accessibility attributes when opened and closed with a click', () => {
      cy.get(form).should('be.hidden')
      cy.get('.search-popup__open').should('have.attr', 'aria-expanded', 'false')
      cy.get('.search-popup__open').should('have.attr', 'aria-controls', 'search-popup-0')
      cy.get(form).should('have.attr', 'id', 'search-popup-0')
      cy.get('.search-popup__open').click()
      cy.get(form).should('be.visible')
      cy.get('.search-popup__open').should('have.attr', 'aria-expanded', 'true')
      cy.get('.search-popup__open').click()
      cy.get(form).should('be.hidden')
      cy.get('.search-popup__open').should('have.attr', 'aria-expanded', 'false')
    })

    it('Make sure the search popup has its accessibility attributes when opened and closed with a focus', () => {
      // Click in and focus backwards
      cy.visit('/patterns/templates-00-homepage/templates-00-homepage.rendered.html');
      cy.get('.search-popup__open').should('have.attr', 'aria-expanded', 'false')
      cy.get(form).should('be.hidden')
      cy.get('.search-popup__open').click()
      cy.get('.search-popup__open').should('have.attr', 'aria-expanded', 'true')
      cy.get(form).should('be.visible')
      cy.get('.search-popup__open').focus()
      cy.get('.site-branding__site-name a').focus()
      cy.get(form).should('be.hidden')
      cy.get('.search-popup__open').should('have.attr', 'aria-expanded', 'false')
      // Click in and focus forwards
      cy.get(form).should('be.hidden')
      cy.get('.search-popup__open').click()
      cy.get('.search-popup__open').should('have.attr', 'aria-expanded', 'true')
      cy.get(form).should('be.visible')
      cy.get('.search-form__submit').focus()
      cy.get('.quick-links__title').focus()
      cy.get(form).should('be.hidden')
      cy.get('.search-popup__open').should('have.attr', 'aria-expanded', 'false')
    })
  })

  context('Mobile', () => {
    beforeEach(function() {
      cy.viewport(320, 600)
    });

    it('Search Popup toggle is hidden in mobile', () => {
      cy.get('.search-popup__open').should('be.hidden')
    })
  })

})
