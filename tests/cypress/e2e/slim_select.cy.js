describe('Slim Select multi select dropdown', () => {
  const menu = '.ss-content'
  const menuOption = '.ss-content .ss-option'
  const toggle = '.ss-values'
  const values = '.ss-values'

  beforeEach(() => {
    cy.visit('/patterns/atoms-forms-01-select-menu/atoms-forms-01-select-menu.rendered.html')
  })

  it('Select dropdown with "multiple" attribute uses the Slim Select library', () => {
    cy.get('.slim-select').should('be.visible')
    cy.get(menu).should('not.be.visible')
    cy.get(toggle).click()
    cy.get(menu).should('be.visible')
  })

  it('Slim Select allows multiple selections', () => {
    cy.get(toggle).click()
    cy.contains(menuOption, 'Option One').click()
    cy.get(toggle).click()
    cy.contains(menuOption, 'Option Two').click()
    cy.get(values)
      .should('have.text', 'Option OneOption Two')

    cy.get(values).contains('Option Two').siblings('.ss-value-delete').click()
    cy.get(values).should('have.text', 'Option One')

    cy.get(toggle).click()
    cy.contains(menuOption, 'Option Five').click()
    cy.get(values).should('have.text', 'Option OneOption Five')
  })
})
