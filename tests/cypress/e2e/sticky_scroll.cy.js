// Note: add a scroll duration so that the scroll event will trigger.
describe('Sticky Scroll Header', () => {
  beforeEach(() => {
    cy.visit('/patterns/pages-01-basic-page/pages-01-basic-page.rendered.html')
  })

  it('Header should be fixed to the top on scroll', () => {
    cy.get('.l-header--fixed').should('not.have.class', 'is-fixed')
    cy.scrollTo('bottom', { duration: 50 })
    cy.get('.l-header--fixed').should('have.class', 'is-fixed')
    cy.scrollTo('top', { duration: 50 })
    cy.get('.l-header--fixed').should('not.have.class', 'is-fixed')
  })

  it('Menu should be fixed to the top on scroll', () => {
    cy.get('[data-cy=navbar]').should('not.have.class', 'is-fixed')
    cy.scrollTo('bottom', { duration: 50 })
    cy.get('[data-cy=navbar]').should('have.class', 'is-fixed')
    cy.scrollTo('top', { duration: 50 })
    cy.get('[data-cy=navbar]').should('not.have.class', 'is-fixed')
  })

  it('Page offset padding should adjust on browser resize', () => {
    cy.scrollTo('bottom', { duration: 50 })
    cy.viewport(1000, 660)
    cy.get('.l-main').invoke('css', 'padding-top')
      .then(originalSizePx => {
        const originalSize = parseInt(originalSizePx, 10)
        cy.viewport(1400, 660)
        cy.wait(50)
        cy.get('.l-main').invoke('css', 'padding-top')
          .then(newSizePx => {
            const newSize = parseInt(newSizePx, 10)
            expect(newSize).to.be.greaterThan(originalSize)
        })
      })
  })

})
