describe('Collapse Accordian', () => {
  beforeEach(() => {
    cy.visit('/patterns/molecules-components-16-collapse/molecules-components-16-collapse.rendered.html')
  })

  it('Click the title to open the content', () => {
    cy.get('[data-cy=content]').should('not.be.visible')
    cy.get('[data-cy=button]').click()
    cy.finishSlide('[data-cy=content]')
    // cy.wait(500)
    cy.get('[data-cy=content]').should('be.visible')
    cy.get('[data-cy=button]').click()
    cy.finishSlide('[data-cy=content]')
    cy.get('[data-cy=content]').should('not.be.visible')
  })

  it('Press enter to open the content', () => {
    cy.get('[data-cy=content]').should('not.be.visible')
    cy.get('[data-cy=button]').focus().type('{enter}')
    cy.finishSlide('[data-cy=content]')
    cy.get('[data-cy=content]').should('be.visible')
    cy.get('[data-cy=button]').focus().type('{enter}')
    cy.finishSlide('[data-cy=content]')
    cy.get('[data-cy=content]').should('not.be.visible')
  })

  it('Accessibility attributes have been added and function on click', () => {
    cy.get('[data-cy=button]').should('have.attr', 'tabindex', '0')
    cy.get('[data-cy=button]').should('have.attr', 'aria-controls', 'collapse-0')
    cy.get('[data-cy=content]').should('have.attr', 'id', 'collapse-0')
    cy.get('[data-cy=button]').should('have.attr', 'aria-expanded', 'false')
    cy.get('[data-cy=button]').click()
    cy.finishSlide('[data-cy=content]')
    cy.get('[data-cy=button]').should('have.attr', 'aria-expanded', 'true')
    cy.get('[data-cy=button]').click()
    cy.finishSlide('[data-cy=content]')
    cy.get('[data-cy=button]').should('have.attr', 'aria-expanded', 'false')
  })
})
