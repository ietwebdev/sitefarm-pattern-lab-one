describe('Primary Navigation', () => {
  const menu = '[data-cy="primaryMenu"]'
  const submenu = '[data-cy="submenu"]'
  const subSubmenu = '[data-cy="subSubmenu"]'
  const toggle = '[data-cy="submenuToggle"]'
  const subToggle = '[data-cy="submenuSubToggle"]'

  beforeEach(() => {
    cy.visit('/patterns/molecules-navigation-00-primary-nav-dropdowns/molecules-navigation-00-primary-nav-dropdowns.rendered.html')
  })

  it('Menu changes based on screen size', () => {
    const listItem = '.menu > li'

    cy.viewport(1000, 660)
    cy.get(menu).should('have.class', 'sf-js-enabled')
    cy.get(listItem).should('have.css', 'float').and('match', /left/)
    cy.viewport(320, 600)
    cy.get(menu).should('not.have.class', 'sf-js-enabled')
    cy.get(listItem).should('have.css', 'float').and('match', /none/)
    cy.viewport(1000, 660)
    cy.get(menu).should('have.class', 'sf-js-enabled')
    cy.get(listItem).should('have.css', 'float').and('match', /left/)
    cy.viewport(320, 600)
    cy.get(menu).should('not.have.class', 'sf-js-enabled')
    cy.get(listItem).should('have.css', 'float').and('match', /none/)
  })

  context('Desktop', () => {
    it('Superfish is enabled', () => {
      cy.get(menu).should('have.class', 'sf-js-enabled')
    })

    it('Hovering over a menu item with submenus should open the submenu', () => {
      // Ensure that the Superfish script has loaded.
      cy.get(menu).should('have.class', 'sf-js-enabled')

      cy.get(submenu).should('not.be.visible')
      // Simulate hovering on the first element by using a Focus.
      cy.get('[data-cy="firstLink"]').focus()
      cy.get(submenu).should('be.visible')

      // Check that the sub-sub menu works
      cy.get(subSubmenu).should('not.be.visible')
      cy.get('[data-cy="submenuFirstLink"]').focus()
      cy.get(subSubmenu).should('be.visible')

      // Moving focus off the submenu closes it
      cy.get('[data-cy="secondLink"]').focus()
      cy.get(subSubmenu).should('not.be.visible')
    })
  })

  context('Mobile', () => {
    beforeEach(function() {
      cy.viewport(320, 600)
    });

    it('Superfish is disabled', () => {
      cy.get(menu).should('not.have.class', 'sf-js-enabled')
    })

    it('Toggle submenu', () => {
      cy.get(submenu).should('not.be.visible')
      cy.get(toggle).click()
      cy.finishSlide(submenu)
      cy.get(submenu).should('be.visible')
      cy.get(toggle).click()
      cy.finishSlide(submenu)
      cy.get(submenu).should('not.be.visible')
    })

    it('Toggle sub-sub menu', () => {
      cy.get(toggle).click()
      cy.finishSlide(submenu)
      cy.get(subSubmenu).should('not.be.visible')
      cy.get(subToggle).click()
      cy.finishSlide(subSubmenu)
      cy.get(subSubmenu).should('be.visible')
      cy.get(subToggle).click()
      cy.finishSlide(subSubmenu)
      cy.get(subSubmenu).should('not.be.visible')
    })
  })

})
