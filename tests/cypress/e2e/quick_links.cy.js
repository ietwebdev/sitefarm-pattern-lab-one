describe('Quick Links', () => {
  const menu = '.quick-links__menu'

  beforeEach(() => {
    cy.visit('/patterns/molecules-navigation-02-quick-links/molecules-navigation-02-quick-links.rendered.html')
  })

  context('Responsive Change', () => {
    it('Menu changes based on screen size', () => {
      cy.viewport(1000, 660)
      cy.get(menu).should('have.css', 'position').and('match', /absolute/)
      cy.viewport(320, 600)
      cy.get(menu).should('have.css', 'position').and('match', /static/)
      cy.viewport(1000, 660)
      cy.get(menu).should('have.css', 'position').and('match', /absolute/)
    })

    it('Open mobile menu closes when moving to desktop', () => {
      cy.viewport(320, 600)
      cy.get('.quick-links__title').click()
      cy.get(menu).should('be.visible')
      cy.viewport(1000, 660)
      cy.get(menu).should('be.hidden')
    })
  })

  context('Desktop', () => {
    it('Quicklinks toggle when clicking the title', () => {
      cy.get(menu).should('be.hidden')
      cy.get('.quick-links__title').click()
      cy.finishSlide(menu)
      cy.get(menu).should('be.visible')
      cy.get('.quick-links__title').click()
      cy.finishSlide(menu)
      cy.get(menu).should('be.hidden')
    })

    it('Quicklinks hide when clicking off the menu', () => {
      cy.visit('/patterns/organisms-global-00-header/organisms-global-00-header.rendered.html')
      cy.get(menu).should('be.hidden')
      cy.get('.quick-links__title').click()
      cy.finishSlide(menu)
      cy.get(menu).should('be.visible')
      cy.get('.header').click()
      cy.get(menu).should('be.hidden')
    })

    it('Quicklinks hide when focus leaves the menu', () => {
      cy.visit('/patterns/organisms-global-00-header/organisms-global-00-header.rendered.html')
      cy.get(menu).should('be.hidden')
      cy.get('.quick-links__title').click()
      cy.finishSlide(menu)
      cy.get(menu).should('be.visible')
      cy.get('.quick-links__menu a').first().focus()
      cy.get('[data-cy=site-name]').focus()
      cy.get(menu).should('be.hidden')
    })
  })

  context('Mobile', () => {
    beforeEach(function() {
      cy.viewport(320, 600)
    });

    it('Menu toggles open on mobile title click', () => {
      cy.get(menu).should('be.hidden')
      cy.get('.quick-links__title').click()
      cy.finishSlide(menu)
      cy.get(menu).should('be.visible')
      cy.get('.quick-links__title').click()
      cy.finishSlide(menu)
      cy.get(menu).should('be.hidden')
    })
  })

})
