describe('Home Page', () => {
  beforeEach(() => {
    cy.visit('/patterns/pages-00-homepage/pages-00-homepage.rendered.html')
  })

  it('Site Name', () => {
    cy.get('[data-cy=site-name]').should('have.text', 'SiteFarm')
  })
})
