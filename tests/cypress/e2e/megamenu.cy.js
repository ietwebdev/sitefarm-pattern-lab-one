describe('MegaMenu', () => {
  const primaryNav = '.primary-nav'
  const submenu = '[data-cy="submenu"]'
  const subSubmenu = '[data-cy="subSubmenu"]'
  const toggle = '[data-cy="submenuToggle"]'
  const subToggle = '[data-cy="submenuSubToggle"]'

  beforeEach(() => {
    cy.visit('/patterns/molecules-navigation-00-primary-nav-megamenu/molecules-navigation-00-primary-nav-megamenu.rendered.html')
  })

  context('Responsive Change', () => {
    it('MegaMenu is being used', () => {
      cy.get('body').should('have.class', 'has-mega')
      cy.get(primaryNav)
        .should('have.class', 'primary-nav--mega')
        .should('have.class', 'primary-nav--justify')
      cy.viewport(320, 600)
      cy.get('body').should('have.class', 'has-mega')
      cy.get(primaryNav)
        .should('have.class', 'primary-nav--mega')
        .should('have.class', 'primary-nav--justify')
    })

    it('Menu changes based on screen size', () => {
      cy.viewport(1000, 660)
      cy.get(primaryNav).should('have.css', 'overflow', 'hidden')
      cy.viewport(320, 600)
      cy.get(primaryNav).should('have.css', 'overflow', 'visible')
      cy.viewport(1000, 660)
      cy.get(primaryNav).should('have.css', 'overflow', 'hidden')
      cy.viewport(320, 600)
      cy.get(primaryNav).should('have.css', 'overflow', 'visible')
    })

    it('The desktop submenus should not be hidden once hidden in mobile', () => {
      cy.viewport(320, 600)
      cy.get(toggle).click()
      cy.finishSlide(submenu)
      cy.get(submenu).should('be.visible')
      cy.get(toggle).click()
      cy.finishSlide(submenu)
      cy.get(submenu).should('be.hidden')
      cy.viewport(1000, 660)
      cy.get(primaryNav).should('have.css', 'overflow', 'hidden')
      // Being hidden with overflow still shows a "visible" in cypress.
      cy.get(submenu).should('be.visible')
    })
  })

  context('Desktop', () => {
    it('Hovering over a menu item with submenus should open the submenu', () => {
      const firstLink = '[data-cy="firstLink"]'

      // Don't check visibility since a height of 0 is still technically visible.
      cy.get(primaryNav).invoke('height')
        .then(height => {
          // Ensure that the megamenu script has loaded.
          cy.get('body').should('have.class', 'has-mega')
          // Simulate hovering on the first element by using a Focus.
          cy.get(firstLink).focus()
          cy.wait(50)
          cy.get(primaryNav).invoke('height')
            .then(openHeight => {
              expect(openHeight).to.be.greaterThan(height)

              cy.get(submenu).should('be.visible')
              cy.get(primaryNav).should('have.class', 'is-hover')

              // Sub-sub menu should be hidden
              cy.get(submenu).find('li .menu').should('be.hidden')

              // Moving focus off the submenu closes it
              cy.get(firstLink).blur()
              cy.get(primaryNav).should('not.have.class', 'is-hover')
              cy.wait(300)
              cy.get(primaryNav).invoke('height')
                .then(closedHeight => {
                  expect(openHeight).to.be.greaterThan(closedHeight)
                })
            })
        })
    })
  })

  context('Mobile', () => {
    beforeEach(function() {
      cy.viewport(320, 600)
    });

    it('MegaMenu is disabled', () => {
      cy.get(primaryNav).should('have.css', 'overflow', 'visible')
    })

    it('Toggle submenu', () => {
      cy.get(submenu).should('not.be.visible')
      cy.get(toggle).click()
      cy.finishSlide(submenu)
      cy.get(submenu).should('be.visible')
      cy.get(toggle).click()
      cy.finishSlide(submenu)
      cy.get(submenu).should('not.be.visible')
    })

    it('Toggle sub-sub menu', () => {
      cy.get(toggle).click()
      cy.finishSlide(submenu)
      cy.get(subSubmenu).should('not.be.visible')
      cy.get(subToggle).click()
      cy.finishSlide(subSubmenu)
      cy.get(subSubmenu).should('be.visible')
      cy.get(subToggle).click()
      cy.finishSlide(subSubmenu)
      cy.get(subSubmenu).should('not.be.visible')
    })
  })

})
