describe('Tab Panel', () => {
  beforeEach(() => {
    cy.visit('/patterns/molecules-components-26-tab-panel/molecules-components-26-tab-panel.rendered.html')
  })

  it('Toggle submenu', () => {
    cy.get('sl-tab[panel="general"]')
    .should('be.visible')
    .should('have.attr', 'active')

    cy.get('sl-tab-panel[name="general"]').contains('This is the general tab panel.').should('be.visible')

    cy.get('sl-tab[panel="custom"]').click()
    cy.get('sl-tab[panel="custom"]')
    .should('be.visible')
    .should('have.attr', 'active')

    cy.get('sl-tab-panel[name="custom"]').contains('This is the custom tab panel.').should('be.visible')

    cy.get('sl-tab[panel="advanced"]').click()
    cy.get('sl-tab[panel="advanced"]')
    .should('be.visible')
    .should('have.attr', 'active')

    cy.get('sl-tab-panel[name="advanced"]').contains('This is the advanced tab panel.').should('be.visible')

    cy.get('sl-tab[panel="disabled"]')
    .should('be.visible')
    .should('have.attr', 'disabled')
  })
})
