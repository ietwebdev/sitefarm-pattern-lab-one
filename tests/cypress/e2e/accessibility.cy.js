describe('Accessibility', () => {
  const ignoreHeadings = {
    rules: {
      'heading-order': { 'enabled': false }
    }
  }

  const ignoreScrollableRegionFocus = {
    rules: {
      'scrollable-region-focusable': { 'enabled': false }
    }
  }

  context('Pages', () => {

    it('Home page', () => {
      cy.visit('/patterns/pages-00-homepage/pages-00-homepage.rendered.html')

      // Inject the axe-core library.
      cy.injectAxe();

      // Test the Accessibility.
      cy.checkA11y({ exclude: ['.slick-dots']}, ignoreHeadings);
    })

    it('Basic page', () => {
      cy.visit('/patterns/pages-01-basic-page/pages-01-basic-page.rendered.html')
      cy.injectAxe();
      cy.checkA11y();
    })

    it('Basic page Featured', () => {
      cy.visit('/patterns/pages-01-basic-page-featured/pages-01-basic-page-featured.rendered.html')
      cy.injectAxe();
      cy.checkA11y();
    })

    it('Article', () => {
      cy.visit('/patterns/pages-02-article/pages-02-article.rendered.html')
      cy.injectAxe();
      cy.checkA11y();
    })

    it('Article Landing', () => {
      cy.visit('/patterns/pages-03-articles-landing/pages-03-articles-landing.rendered.html')
      cy.injectAxe();
      cy.checkA11y();
    })

    it('Event', () => {
      cy.visit('/patterns/pages-04-event/pages-04-event.rendered.html')
      cy.injectAxe();
      cy.checkA11y();
    })

    it('Event Landing', () => {
      cy.visit('/patterns/pages-05-event-landing/pages-05-event-landing.rendered.html')
      cy.injectAxe();
      cy.checkA11y();
    })

    it('Taxonomy Term', () => {
      cy.visit('/patterns/pages-06-taxonomy-term/pages-06-taxonomy-term.rendered.html')
      cy.injectAxe();
      // Ignore Heading Order since this can't be changed.
      cy.checkA11y({}, ignoreHeadings);
    })

    it('Person', () => {
      cy.visit('/patterns/pages-07-person/pages-07-person.rendered.html')
      cy.injectAxe();
      cy.checkA11y();
    })

    it('Person Directory', () => {
      cy.visit('/patterns/pages-08-person-directory/pages-08-person-directory.rendered.html')
      cy.injectAxe();
      cy.checkA11y({}, {
        // Ignore aria input on multiselect since it is controlled by plugin.
        // TODO: delete this when https://github.com/brianvoe/slim-select/pull/292
        // is merged and released. Other issues need to be addressed in the
        // plugin as well.
        rules: {
          'aria-input-field-name': { 'enabled': false },
          'aria-required-children': { 'enabled': false },
          'region': { 'enabled': false }
        }
      });
    })

    it('Search', () => {
      cy.visit('/patterns/pages-09-search/pages-09-search.rendered.html')
      cy.injectAxe();
      cy.checkA11y();
    })

    it('Photo Gallery', () => {
      cy.visit('/patterns/pages-10-photo-gallery/pages-10-photo-gallery.rendered.html')
      cy.injectAxe();
      cy.checkA11y();
    })

    it('Photo Gallery Landing', () => {
      cy.visit('/patterns/pages-11-photo-gallery-landing/pages-11-photo-gallery-landing.rendered.html')
      cy.injectAxe();
      cy.checkA11y();
    })

    it('CAS Login', () => {
      cy.visit('/patterns/pages-12-cas-login/pages-12-cas-login.rendered.html')
      cy.injectAxe();
      cy.checkA11y();
    })

  })

  context('Branding Elements', () => {

    // it('Branding Text', () => {
    //   cy.visit('/patterns/organisms-branding-00-branding-text/organisms-branding-00-branding-text.rendered.html')
    //   cy.injectAxe();
    //   cy.checkA11y({}, ignoreHeadings);
    // })

    it('Branding Titles', () => {
      cy.visit('/patterns/organisms-branding-01-branding-titles/organisms-branding-01-branding-titles.rendered.html')
      cy.injectAxe();
      cy.checkA11y({}, ignoreHeadings);
    })

    it('Branding Collapse', () => {
      cy.visit('/patterns/organisms-branding-02-branding-collapse/organisms-branding-02-branding-collapse.rendered.html')
      cy.injectAxe();
      cy.checkA11y({}, ignoreHeadings);
    })

    it('Branding Article Teasers', () => {
      cy.visit('/patterns/organisms-branding-03-branding-teasers-article/organisms-branding-03-branding-teasers-article.rendered.html')
      cy.injectAxe();
      cy.checkA11y({}, ignoreHeadings);
    })

    it('Branding Event Teasers', () => {
      cy.visit('/patterns/organisms-branding-03-branding-teasers-event/organisms-branding-03-branding-teasers-event.rendered.html')
      cy.injectAxe();
      cy.checkA11y({}, ignoreHeadings);
    })

    it('Branding Person Teasers', () => {
      cy.visit('/patterns/organisms-branding-03-branding-teasers-person/organisms-branding-03-branding-teasers-person.rendered.html')
      cy.injectAxe();
      cy.checkA11y({}, ignoreHeadings);
    })

    it('Branding Poster VM', () => {
      cy.visit('/patterns/organisms-branding-04-branding-poster-vm/organisms-branding-04-branding-poster-vm.rendered.html')
      cy.injectAxe();
      cy.checkA11y({}, ignoreHeadings);
    })

    it('Branding Spotlight VM', () => {
      cy.visit('/patterns/organisms-branding-05-branding-spotlight-vm/organisms-branding-05-branding-spotlight-vm.rendered.html')
      cy.injectAxe();
      cy.checkA11y({}, ignoreHeadings);
    })

    it('Branding Focus Box', () => {
      cy.visit('/patterns/organisms-branding-06-branding-focus-box/organisms-branding-06-branding-focus-box.rendered.html')
      cy.injectAxe();
      cy.checkA11y({}, ignoreHeadings);
    })

    it('Branding Textbox', () => {
      cy.visit('/patterns/organisms-branding-07-branding-textbox/organisms-branding-07-branding-textbox.rendered.html')
      cy.injectAxe();
      cy.checkA11y({}, ignoreHeadings);
    })

    it('Branding Marketing Highlight', () => {
      cy.visit('/patterns/organisms-branding-08-branding-marketing-highlight/organisms-branding-08-branding-marketing-highlight.rendered.html')
      cy.injectAxe();
      cy.checkA11y({}, ignoreHeadings);
    })

    it('Branding Marketing Highlight Featured', () => {
      cy.visit('/patterns/organisms-branding-08-branding-marketing-hightlight-featured/organisms-branding-08-branding-marketing-hightlight-featured.rendered.html')
      cy.injectAxe();
      cy.checkA11y({}, ignoreHeadings);
    })

    it('Marketing Highlight Horizontal', () => {
      cy.visit('/patterns/organisms-branding-09-marketing-highlight-horizontal/organisms-branding-09-marketing-highlight-horizontal.rendered.html')
      cy.injectAxe();
      cy.checkA11y({}, ignoreHeadings);
    })

    it('Branding Block Quotes', () => {
      cy.visit('/patterns/organisms-branding-12-branding-block-quote/organisms-branding-12-branding-block-quote.rendered.html')
      cy.injectAxe();
      cy.checkA11y({}, ignoreScrollableRegionFocus);
    })

    it('Branding Pull Quotes', () => {
      cy.visit('/patterns/organisms-branding-13-branding-pull-quote/organisms-branding-13-branding-pull-quote.rendered.html')
      cy.injectAxe();
      cy.checkA11y({}, ignoreScrollableRegionFocus);
    })

    it('Layout Sections', () => {
      cy.visit('/patterns/organisms-branding-14-layout-section/organisms-branding-14-layout-section.rendered.html')
      cy.injectAxe();
      cy.checkA11y({}, ignoreScrollableRegionFocus);
    })

    it('Alignable Promo', () => {
      cy.visit('/patterns/organisms-branding-15-branding-alignable-promo/organisms-branding-15-branding-alignable-promo.rendered.html')
      cy.injectAxe();
      cy.checkA11y({}, ignoreScrollableRegionFocus);
    })

    it('Branding Tile Links', () => {
      cy.visit('/patterns/organisms-branding-16-branding-tile-link/organisms-branding-16-branding-tile-link.rendered.html')
      cy.injectAxe();
      cy.get("a.tile-link").realHover();
      cy.checkA11y({}, ignoreHeadings);
    })


    it('Branding Photo Cards', () => {
      cy.visit('/patterns/organisms-branding-17-branding-photo-card/organisms-branding-17-branding-photo-card.rendered.html')
      cy.injectAxe();
      cy.checkA11y({}, ignoreHeadings);
    })

  })

  context('Messaging', () => {

    it('Alert', () => {
      cy.visit('/patterns/molecules-messaging-00-alert/molecules-messaging-00-alert.rendered.html')
      cy.injectAxe();
      cy.checkA11y('.alert');
    })

    it('Success', () => {
      cy.visit('/patterns/molecules-messaging-01-alert-success/molecules-messaging-01-alert-success.rendered.html')
      cy.injectAxe();
      cy.checkA11y('.alert');
    })

    it('Warning', () => {
      cy.visit('/patterns/molecules-messaging-02-alert-warning/molecules-messaging-02-alert-warning.rendered.html')
      cy.injectAxe();
      cy.checkA11y('.alert');
    })

    it('Error', () => {
      cy.visit('/patterns/molecules-messaging-03-alert-error/molecules-messaging-03-alert-error.rendered.html')
      cy.injectAxe();
      cy.checkA11y('.alert');
    })
  })
})
