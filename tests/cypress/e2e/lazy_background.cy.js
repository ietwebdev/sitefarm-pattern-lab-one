describe('Lazy Loaded Background', () => {
  it('Generic Lazy Background Loads', () => {
    cy.visit('/patterns/atoms-media-03-lazy-background/atoms-media-03-lazy-background.rendered.html')
    cy.contains('Lazy Loaded Background')
    cy.get('.lazyload__bg')
      .should('have.css', 'background-image')
      .and('match', /images\/placeholders\/1280x720.png/)
  })

  it('Layout Section has Lazy Background Image', () => {
    cy.visit('/patterns/molecules-layout-section-02-lazy-background-image/molecules-layout-section-02-lazy-background-image.rendered.html')
    cy.contains('Lazy Loaded')
    cy.get('.layout-section__bg-image')
      .should('have.css', 'background-image')
      .and('match', /images\/placeholders\/1280x720.png/)
  })
})
