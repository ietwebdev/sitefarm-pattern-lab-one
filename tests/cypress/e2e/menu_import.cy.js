describe('Menu Import', () => {
  const ResponseText = 'External menu added'

  beforeEach(() => {
    // Stub the request for the menu
    cy.intercept(
      {
        method: 'GET',
        url: 'https://www.ucdavis.edu/menu/menu-quick-links',
      },
      ResponseText
    )

    cy.visit('/patterns/molecules-navigation-02-quick-links-home-site/molecules-navigation-02-quick-links-home-site.rendered.html', {
      onBeforeLoad: (win) => {
        win.sessionStorage.clear()
      }
    })
  })

  it('Ajax in a menu if there is not one in session storage', () => {
    cy.get('#quick-links').contains(ResponseText)
      .then(() => {
        expect(sessionStorage.getItem('menu-menu-quick-links')).to.contain(ResponseText)
      })
  })

  it('External menu will pull from Session storage if available', () => {
    const sessionText = 'pulled from storage'
    cy.visit('/patterns/molecules-navigation-02-quick-links-home-site/molecules-navigation-02-quick-links-home-site.rendered.html', {
      onBeforeLoad: (win) => {
        win.sessionStorage.setItem('menu-menu-quick-links', sessionText)
      }
    })
    cy.get('#quick-links').contains(sessionText)
  })
})
