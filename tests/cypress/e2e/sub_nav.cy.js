describe('Sub Nav', () => {

  beforeEach(() => {
    cy.visit('/patterns/molecules-navigation-01-sub-nav/molecules-navigation-01-sub-nav.rendered.html')
  })

  it('Toggle submenu', () => {
    const toggle = '[data-cy=subnav-toggle]'
    const subMenu = '[data-cy=subnav-submenu]'

    cy.get(subMenu).should('not.be.visible')
    cy.get(toggle).click()
    cy.finishSlide(subMenu)
    cy.get(subMenu).should('be.visible')
    cy.get(toggle).click()
    cy.finishSlide(subMenu)
    cy.get(subMenu).should('not.be.visible')
  })

  it('Toggle sub-sub menu', () => {
    const toggle = '[data-cy=subnav-toggle]'
    const subMenu = '[data-cy=subnav-submenu]'
    const subToggle = '[data-cy=subnav-child-toggle]'
    const subSubMenu = '[data-cy=subnav-sub-submenu]'

    cy.get(toggle).click()
    cy.finishSlide(subMenu)
    cy.get(subSubMenu).should('not.be.visible')
    cy.get(subToggle).click()
    cy.finishSlide(subSubMenu)
    cy.get(subSubMenu).should('be.visible')
    cy.get(subToggle).click()
    cy.finishSlide(subSubMenu)
    cy.get(subSubMenu).should('not.be.visible')
  })

})
