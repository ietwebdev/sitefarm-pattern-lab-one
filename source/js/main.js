import aggieVideoPlaylist from './aggie-video-playlist.js';
import collapse from './collapse.js';
import heroBanner from './hero-banner-popup-video.js';
import lazyBackgroundImage from './lazy-background-image.js';
import lazyloadImages from './lazyload-images.js';
import listAccordion from './list-accordion.js';
import listFaq from './list-faq.js';
import menuImport from './menu-import.js';
import brandTextbox from './brand-textbox.js';
import mobileNavToggle from './mobile-nav-toggle.js';
import pageWidth from './page-width.js';
import panelMobileCollapse from './panel-mobile-collapse.js';
import photoSwipe from './photoswipe.js';
import primaryNav from './primary-nav.js';
import quickLinks from './quick-links.js';
import responsiveMediaAlignment from './responsive-media-alignment.js'
import searchPopup from './search-popup.js';
import slideshow from './slideshow.js';
import slimSelect from './slim-select.js';
import stickyScroll from './sticky-scroll.js';
import subNav from './sub-nav.js';
import weightedTitle from './weighted-title.js';

// Web Components.
import './components/layout-columns.js';

pageWidth();
aggieVideoPlaylist();
collapse();
heroBanner();
lazyBackgroundImage();
lazyloadImages();
listAccordion();
listFaq();
menuImport();
brandTextbox();
mobileNavToggle();
panelMobileCollapse();
photoSwipe();
primaryNav();
quickLinks();
responsiveMediaAlignment();
searchPopup();
slideshow();
slimSelect();
stickyScroll();
subNav();
weightedTitle();
