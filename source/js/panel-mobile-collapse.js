import { toggle } from 'slide-element';
import { toggleAriaExpanded } from './lib/accessiblity-functions.js';

// Panel Mobile Collapse
export default function panelMobileCollapse() {

  const mqMediumUp = window.matchMedia('(min-width: 992px)');
  const panelTitles = document.querySelectorAll('.panel--mobile-collapse .panel__title');
  const panelContent = document.querySelectorAll('.panel--mobile-collapse .panel__content');

  const mobileSwitch = (mql) => {
    // Desktop
    if (mql.matches) {
      panelContent.forEach(content => {
        content.removeAttribute('style')
        content.style.display = 'block';
      })

      // Remove click bind from title
      panelTitles.forEach(title => {
        title.removeAttribute('aria-expanded');
        title.removeEventListener('click', togglePanel);
        title.removeEventListener('keypress', togglePanel);
      });
    }

    // Mobile
    else {
      // First: hide content
      panelContent.forEach(content => {
        content.style.display = 'none';
      })

      panelTitles.forEach(title => {
        title.setAttribute('aria-expanded', 'false');
        title.addEventListener('click', togglePanel);
        title.addEventListener('keypress', togglePanel);
      });
    }
  };

  function togglePanel(event) {
    if (event.keyCode === 13 || event.type === 'click') {
      let panel = this.nextElementSibling;
      // The next sibling may not be the panel in Drupal. Drupal may put in a
      // contextual links div in between.
      if (!panel.classList.contains('.panel__content')) {
        panel = this.nextElementSibling;
      }

      toggleAriaExpanded(this);
      toggle(panel);
    }
  }

  // Watch for changes to the browser size
  mqMediumUp.addListener(mobileSwitch);
  // On Load
  mobileSwitch(mqMediumUp);

}
