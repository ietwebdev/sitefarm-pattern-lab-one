import $ from 'jquery';
import 'slick-carousel';

export default function slideshow() {

  // Exit if the Slick library has not been loaded
  if (!$.isFunction($.fn.slick)) {
    return;
  }

  $(document).ready(() => {
    const $nav = $('.slider-nav');

    const mainOptions = {
      lazyLoad: 'ondemand',
      slidesToShow: 1,
      slidesToScroll: 1,
      fade: true,
      dots: true
    };

    // Only set an "asNavFor" setting if there is actually a navigation element.
    if ($nav.length > 0) {
      mainOptions.asNavFor = '.slider-nav';
    }

    $('.slideshow').slick(mainOptions);
    $nav.slick({
      lazyLoad: 'ondemand',
      slidesToShow: 3,
      slidesToScroll: 1,
      asNavFor: '.slideshow',
      dots: false,
      centerMode: true,
      centerPadding: '70px',
      focusOnSelect: true,
      arrows: false
    });
  });

}
