const domain = 'www.ucdavis.edu';

export default function menuImport() {

  const menus = document.querySelectorAll('[data-menu-name]');

  menus.forEach(menu => {
    // Get the menu name
    const { menuName } = menu.dataset;
    const menuStorageKey = `menu-${menuName}`;

    // Check to see if the menu data is already stored.
    const menuData = sessionStorage.getItem(menuStorageKey);

    if (menuData) {
      menu.outerHTML = menuData;
    } else {
      fetch(`https://${domain}/menu/${menuName}`)
        .then(response => response.text())
        .then(data => {
          menu.outerHTML = data;
          // Store the menu data to a session storage.
          sessionStorage.setItem(menuStorageKey, data);
        });
    }
  })

}
