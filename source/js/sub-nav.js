import submenuToggle from './lib/submenu-toggle.js';

export default function subNav() {

  const menus = document.querySelectorAll('.sub-nav');

  menus.forEach(menu => {
    const buttons = menu.querySelectorAll('.submenu-toggle');
    const menuSub = menu.querySelectorAll('ul ul');

    // Hide submenus if there is not a data attribute set to open them at start.
    if (menu.dataset.menuOpen !== undefined) {
      // Add a class signifying that the submen is already open.
      buttons.forEach(item => {
        item.classList.add('submenu-toggle--open')
      })
    } else {
      // Hide submenus.
      menuSub.forEach(item => {
        item.style.display = 'none';
      })
    }

    // Toggle submenu when clicked
    submenuToggle(buttons);
  })

}
