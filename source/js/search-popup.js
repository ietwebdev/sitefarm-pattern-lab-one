import {setCollapseAccessibility, toggleAriaExpanded} from './lib/accessiblity-functions.js';

export default function searchPopup() {

  const popup = document.querySelector('.search-popup');
  const btnOpen = document.querySelector('.js-search-popup__open');
  const input = document.querySelector('.search-popup .search-form__input');
  const mqMediumUp = window.matchMedia('(min-width: 992px)');

  if (!popup) {
    return;
  }

  // Set aria attributes on popup if not mobile
  if (mqMediumUp.matches) {
    setCollapseAccessibility('.js-search-popup__open', '.search-popup', 'search-popup');
  }

  // Use the button to toggle the search drop down open or closed using click or enter
  btnOpen.addEventListener('click', dropdownToggle);
  btnOpen.addEventListener('keypress', dropdownToggle);

  // Hide the search popup form when focus leaves the popup area.
  popup.addEventListener('focusout', () => {
    // Wait to ensure the newly focused element is outside the search form.
    setTimeout(function () {
      if (document.activeElement === document.body || document.activeElement === null) {
        return;
      }

      // Close the search box if the currently focused element is not inside the search box.
      if (document.activeElement.closest('.search-popup') === null) {
        dropdownClose();
      }
    }, 150);
  })

  // Hide the search popup form when clicking outside the popup area.
  document.addEventListener('click', event => {
    // Ignore clicks on the search popup itself or the toggle button.
    if (event.target.closest('.search-popup, .js-search-popup__open')) {
      return;
    }

    dropdownClose();
  })

  // Toggle the search dropdown open or closed.
  function dropdownToggle(event) {
    if (event.keyCode === 13 || event.type === 'click') {
      popup.classList.toggle('is-open');
      // Toggle the magnifying glass and X on the search popup button.
      btnOpen.classList.toggle('search-popup__open--close');

      // Toggle the aria expanded attribute for accessibility.
      if (mqMediumUp.matches) {
        toggleAriaExpanded(btnOpen);
      }

      // Move the focus to the search when popup opens
      if (popup.classList.contains('is-open')) {
        input.focus();
      }
    }
    event.preventDefault();
  }

  function dropdownClose() {
    popup.classList.remove('is-open');
    btnOpen.classList.remove('search-popup__open--close');

    // Toggle the aria expanded attribute for accessibility
    if (mqMediumUp.matches) {
      btnOpen.setAttribute('aria-expanded', 'false');
    }
  }

}
