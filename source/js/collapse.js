import { toggle } from 'slide-element';
import { setCollapseAccessibility, toggleAriaExpanded } from './lib/accessiblity-functions.js';

export default function collapse() {

  const titles = document.querySelectorAll('.collapse__title');
  const content = document.querySelectorAll('.collapse__content');

  setCollapseAccessibility('.collapse__title', '.collapse__content', 'collapse');

  // Accessibility: First remove any visually hidden classes and hide content
  content.forEach(item => {
    item.style.display = 'none';
    item.classList.remove('u-hidden--visually');
  });

  titles.forEach(title => {
    title.addEventListener('click', toggleCollapse);
    title.addEventListener('keypress', toggleCollapse);
  });

  function toggleCollapse(event) {
    if (event.keyCode === 13 || event.type === 'click') {
      const panel = this.nextElementSibling;

      this.classList.toggle('collapse__title--open');
      this.classList.toggle('collapse__title--closed');
      toggleAriaExpanded(this);
      toggle(panel);
    }
  }

}
