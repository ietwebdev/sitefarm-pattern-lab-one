import { toggle } from 'slide-element';

/**
 * Toggle submenu when clicked.
 *
 * @param {NodeList} buttons
 *   List of the button elements.
 */
export default function submenuToggle(buttons) {
  buttons.forEach(button => {
    button.addEventListener('click', function () {
      const wrapper = this.closest('.submenu-toggle__wrapper');
      const menu = wrapper.nextElementSibling;

      this.classList.toggle('submenu-toggle--open');
      toggle(menu);
    });
  });
}

/**
 * Remove SlideToggle styles from submenus.
 *
 * @param {NodeList} submenus
 *   List of submenu elements.
 */
export function removeToggleStyles(submenus) {
  submenus.forEach(menu => {
    menu.removeAttribute('style');
  });
}
