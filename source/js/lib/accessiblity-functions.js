export function setCollapseAccessibility(buttonSelector, contentSelector, uniqueIdentifier, ariaExpandedStart = 'false') {

  const buttons = document.querySelectorAll(buttonSelector);
  const content = document.querySelectorAll(contentSelector);

  for (let i = 0; i < buttons.length; ++i) {
    buttons[i].setAttribute('aria-controls', `${uniqueIdentifier}-${i}`);
    buttons[i].setAttribute('aria-expanded', ariaExpandedStart);
    buttons[i].setAttribute('tabindex', 0);
  }

  for (let i = 0; i < content.length; ++i) {
    content[i].setAttribute('id', `${uniqueIdentifier}-${i}`);
  }
}

export function toggleAriaExpanded(button) {
  if (button.getAttribute('aria-expanded') === 'false') {
    button.setAttribute('aria-expanded', 'true');
  }
  else {
    button.setAttribute('aria-expanded', 'false');
  }
}
