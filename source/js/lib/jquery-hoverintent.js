// Copied the skypack esm version of jquery-hoverintent so that it can be
// imported in the primary-nav.js file. The normal npm package had issues.
// We changed the skypack version of jquery to our own local version.
// https://cdn.skypack.dev/pin/jquery-hoverintent@v1.10.2-cW2GwN9PcnjiMsKiNYEk/mode=imports/optimized/jquery-hoverintent.js
import require$$0 from "jquery";
function createCommonjsModule(fn, basedir, module) {
  return module = {
    path: basedir,
    exports: {},
    require: function(path, base) {
      return commonjsRequire(path, base === void 0 || base === null ? module.path : base);
    }
  }, fn(module, module.exports), module.exports;
}
function commonjsRequire() {
  throw new Error("Dynamic requires are not currently supported by @rollup/plugin-commonjs");
}
var jquery_hoverIntent = createCommonjsModule(function(module) {
  (function(factory) {
    if (module.exports) {
      module.exports = factory(require$$0);
    } else if (jQuery && !jQuery.fn.hoverIntent) {
      factory(jQuery);
    }
  })(function($) {
    var _cfg = {
      interval: 100,
      sensitivity: 6,
      timeout: 0
    };
    var INSTANCE_COUNT = 0;
    var cX, cY;
    var track = function(ev) {
      cX = ev.pageX;
      cY = ev.pageY;
    };
    var compare = function(ev, $el, s, cfg) {
      if (Math.sqrt((s.pX - cX) * (s.pX - cX) + (s.pY - cY) * (s.pY - cY)) < cfg.sensitivity) {
        $el.off(s.event, track);
        delete s.timeoutId;
        s.isActive = true;
        ev.pageX = cX;
        ev.pageY = cY;
        delete s.pX;
        delete s.pY;
        return cfg.over.apply($el[0], [ev]);
      } else {
        s.pX = cX;
        s.pY = cY;
        s.timeoutId = setTimeout(function() {
          compare(ev, $el, s, cfg);
        }, cfg.interval);
      }
    };
    var delay = function(ev, $el, s, out) {
      var data = $el.data("hoverIntent");
      if (data) {
        delete data[s.id];
      }
      return out.apply($el[0], [ev]);
    };
    var isFunction = function(value) {
      return typeof value === "function";
    };
    $.fn.hoverIntent = function(handlerIn, handlerOut, selector) {
      var instanceId = INSTANCE_COUNT++;
      var cfg = $.extend({}, _cfg);
      if ($.isPlainObject(handlerIn)) {
        cfg = $.extend(cfg, handlerIn);
        if (!isFunction(cfg.out)) {
          cfg.out = cfg.over;
        }
      } else if (isFunction(handlerOut)) {
        cfg = $.extend(cfg, {over: handlerIn, out: handlerOut, selector});
      } else {
        cfg = $.extend(cfg, {over: handlerIn, out: handlerIn, selector: handlerOut});
      }
      var handleHover = function(e) {
        var ev = $.extend({}, e);
        var $el = $(this);
        var hoverIntentData = $el.data("hoverIntent");
        if (!hoverIntentData) {
          $el.data("hoverIntent", hoverIntentData = {});
        }
        var state = hoverIntentData[instanceId];
        if (!state) {
          hoverIntentData[instanceId] = state = {id: instanceId};
        }
        if (state.timeoutId) {
          state.timeoutId = clearTimeout(state.timeoutId);
        }
        var mousemove = state.event = "mousemove.hoverIntent.hoverIntent" + instanceId;
        if (e.type === "mouseenter") {
          if (state.isActive) {
            return;
          }
          state.pX = ev.pageX;
          state.pY = ev.pageY;
          $el.off(mousemove, track).on(mousemove, track);
          state.timeoutId = setTimeout(function() {
            compare(ev, $el, state, cfg);
          }, cfg.interval);
        } else {
          if (!state.isActive) {
            return;
          }
          $el.off(mousemove, track);
          state.timeoutId = setTimeout(function() {
            delay(ev, $el, state, cfg.out);
          }, cfg.timeout);
        }
      };
      return this.on({"mouseenter.hoverIntent": handleHover, "mouseleave.hoverIntent": handleHover}, cfg.selector);
    };
  });
});
export default jquery_hoverIntent;
