import SlimSelect from 'slim-select';

export default function slimSelect() {
  const multiSelects = document.querySelectorAll('.slim-select');

  multiSelects.forEach(item => {
    new SlimSelect({
      select: item
    });
  })

}
