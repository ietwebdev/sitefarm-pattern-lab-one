// Determine if an element is too large for its parent container.
const className = 'u-align--stack';
const ignoreSize = 50;

/**
 * Adds a class to media elements that are too wide for their parent container.
 *
 * This function observes the width of the parent element of each media element
 * specified by the selectors. If a media element's width exceeds the parent's
 * width minus the offset, a specific class is added to the element. The
 * function runs both on page load and whenever the parent element is resized.
 *
 * @param {number} offset
 *   How much room should be left at the side of the element for other content.
 * @param {string} selectors
 *   The css selectors to use when querying for media elements.
 */
export default function responsiveMediaAlignment(offset = 125, selectors = '.u-align--right, .u-align--left, .align-left, .align-right') {
  const elements = document.querySelectorAll(selectors);

  elements.forEach(element => {
    let mediaElement;
    const { parentElement } = element;

    const tags = ['img', 'video', 'iframe'];
    const tagName = element.tagName.toLowerCase();
    if (tags.includes(tagName)) {
      mediaElement = element;
    } else {
      // Find the element's nearest media descendant.
      mediaElement = element.querySelector(tags.join(', '));
    }

    if (!mediaElement) {
      return;
    }

    // Ignore mediaElements not large enough to bother with.
    const mediaElementWidth = mediaElement.getAttribute('width');
    if (mediaElementWidth && mediaElementWidth < ignoreSize && mediaElement.clientWidth < ignoreSize) {
      return;
    }

    const alignmentCheck = checkAlignment(element, parentElement, mediaElement, offset);

    // Run on page load.
    alignmentCheck();

    // Run on resize.
    const resizeObserver = new ResizeObserver(alignmentCheck);
    resizeObserver.observe(parentElement);
  });
}

/**
 * Check if an element is too wide for its parent container and set a class.
 *
 * @param {HTMLElement} element The element to check.
 * @param {HTMLElement} parentElement The parent element to compare against.
 * @param {HTMLElement} mediaElement The mediaElement element to check.
 * @param {number} offset How much room should be left next to the element.
 *
 * @return {Function} A function to check the alignment.
 */
function checkAlignment(element, parentElement, mediaElement, offset) {
  let lastObservedWidth;

  return () => {
    const mediaWidth = mediaElement.offsetWidth;
    const parentWidth = parentElement.clientWidth;

    // If the width hasn't changed, do nothing.
    if (parentWidth === lastObservedWidth) {
      return;
    }
    // Update the last observed width.
    lastObservedWidth = parentWidth;

    if (mediaWidth > parentWidth - offset) {
      element.classList.add(className);
    } else {
      element.classList.remove(className);
    }
  };
}
