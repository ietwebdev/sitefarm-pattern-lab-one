export default function heroBanner() {

  // This is assuming a Youtube or Vimeo iframe embed is present
  const heroBanners = document.querySelectorAll('.hero-banner');

  heroBanners.forEach(banner => {
    // Exit if a video button is not present.
    const videoContainer = banner.querySelector('.hero-banner__video-popup');
    if (!videoContainer) {
      return;
    }

    const toHide = banner.querySelectorAll('.hero-banner__body, .hero-banner__image');
    const video = videoContainer.querySelector('iframe');
    const { src } = video.dataset;
    let queryCharacter = '?';

    // Append the autoplay tag as a new parameter or appended to existing params
    if (src.includes('?')) {
      queryCharacter = '&';
    }

    const videoBtn = banner.querySelector('.hero-banner__video-play');
    videoBtn.addEventListener('click', event => {
      event.preventDefault();
      event.stopPropagation();

      video.setAttribute('src', `${src + queryCharacter}autoplay=1`);

      // Show the Video
      videoContainer.classList.remove('u-hidden');

      // Hide Everything else
      toHide.forEach(element => {
        element.classList.add('u-hidden');
      })
    })

  })

}
