export default function mobileNavToggle() {

  const menuToggle = document.querySelector('.js-nav-toggle');
  const menuNavbar = document.querySelector('.l-navbar');

  // Exit if there is no primary menu.
  if (!menuNavbar) {
    return;
  }

  const mqMediumUp = window.matchMedia('(min-width: 992px)');
  let hideMenuDelay;

  // Hide the menu initially when in mobile.
  if (!mqMediumUp.matches) {
    menuNavbar.classList.add('menu--hidden');
  }

  // Hide the menu so items are not focusable. Delay it so that it doesn't
  // become hidden until it slides off the screen.
  const hideMenu = () => {
    hideMenuDelay = setTimeout(function () {
      menuNavbar.classList.add('menu--hidden');
    }, 500);
  }

  // Slide the menu closed.
  const closeMenu = () => {
    menuToggle.classList.remove('nav-toggle--active');
    menuToggle.setAttribute('aria-expanded', 'false');
    menuNavbar.classList.remove('menu--open');
    menuNavbar.classList.add('menu--closed');
    hideMenu();
  }

  // Slide the menu open.
  const openMenu = () => {
    menuToggle.classList.add('nav-toggle--active');
    menuToggle.setAttribute('aria-expanded', 'true');
    menuNavbar.classList.add('menu--open');
    menuNavbar.classList.remove('menu--closed');

    // Remove the hidden state before focusing inside the menu.
    clearTimeout(hideMenuDelay);
    menuNavbar.classList.remove('menu--hidden');
  }

  // Toggle open and close the off-canvas menu.
  menuToggle.addEventListener('click', () => {
    if (menuNavbar.classList.contains('menu--open')) {
      closeMenu();
    } else {
      openMenu();
    }
  });

  // Hide the off-canvas menu when leaving focus.
  menuNavbar.addEventListener('focusout', () => {
    if (mqMediumUp.matches) {
      return;
    }

    // Wait to ensure the newly focused element is outside the off-canvas tray.
    setTimeout(function () {
      // Close the off-canvas menu if the previous focus was in the menu and
      // The menu toggle is not being clicked again.
      if (document.activeElement === document.body ||
        document.activeElement === null ||
        document.activeElement.closest('.l-navbar') !== null ||
        document.activeElement.closest('.js-nav-toggle')
      ) {
        return;
      }
      closeMenu();
    }, 150);
  })

}
