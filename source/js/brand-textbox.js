import {setCollapseAccessibility, toggleAriaExpanded} from './lib/accessiblity-functions.js';

export default function brandTextbox() {

  const buttons = document.querySelectorAll('.brand-textbox__button');

  setCollapseAccessibility('.brand-textbox__button', '.brand-textbox__content', 'msg-area', true);
  hideClosedMsgAreas();

  // Toggle collapse on click or pressing enter
  buttons.forEach(button => {
    button.addEventListener('click', toggleCollapse);
  });

  function toggleCollapse() {

    const block = this.closest('.message-area');

    // Make sure the brand textbox has an ID otherwise it causes js issues
    if (block.id && block.id !== '') {
      block.querySelector('.brand-textbox').classList.toggle('brand-textbox--closed');

      toggleAriaExpanded(this);
      toggleSessionData(block.id);
    }
  }

  function collapseBlock(id) {
    const block = document.getElementById(id);

    // If block is null it will throw a JS error
    if (block) {
      const button = block.querySelector('.brand-textbox__button');

      block.querySelector('.brand-textbox').classList.add('brand-textbox--closed');

      toggleAriaExpanded(button);
    }
  }

  function toggleSessionData(blockId) {
    const sfBlockId = `sf-brand-textbox--${blockId}`;
    const blockStatus = sessionStorage.getItem(sfBlockId);

    if (typeof blockStatus === 'undefined' || blockStatus === null) {
      sessionStorage.setItem(sfBlockId, 'close');
    } else {
      sessionStorage.removeItem(sfBlockId);
    }
  }

  function hideClosedMsgAreas() {
    // Loop through session items
    Object.keys(sessionStorage).forEach(key => {
      const brandTextBoxSessionData = key.includes('sf-brand-textbox--');
      const id = key.replace('sf-brand-textbox--', '');
      const blockStatus = sessionStorage.getItem(key);

      if (brandTextBoxSessionData && blockStatus === 'close') {
        collapseBlock(id);
      }
    });
  }
}
