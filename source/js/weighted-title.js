export default function weightedTitle() {

  const weightedClass = 'panel__title--weighted';
  const titles = document.querySelectorAll('.panel--weighted-underline .panel__title');

  // Wrap the first word of each weighted title with a span
  titles.forEach(title => {
    const text = title.innerHTML;
    let newText;
    const pattern = /^((\S{3,})|(\S{1,2}\s\S+))/;
    const matches = text.match(pattern);

    if (matches) {
      newText = text.replace(pattern, `<span class="${weightedClass}">${matches[0]}</span>`);
    }
    else {
      newText = `<span class="${weightedClass}">${text}</span>`;
    }
    title.innerHTML = newText;
  });
}
