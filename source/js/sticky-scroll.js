export default function stickyScroll() {

  const fixed = document.querySelector('.l-header--fixed');

  // Exit if the site is not using a fixed header.
  if (!fixed) {
    return;
  }

  const root = document.documentElement;
  const navbar = document.querySelector('.l-navbar');
  const topLink = document.querySelector('.primary-nav__top-link');
  const mega = document.querySelector('.primary-nav--mega');
  const headerBranding = document.querySelector('.l-header__branding');
  const headerImages = headerBranding.querySelectorAll('img');
  const mqMediumUp = window.matchMedia('(min-width: 992px)');

  // Get the distance from the navbar to the top of the page when stationary.
  let navPosition;
  const setNavPosition = () => {
    navPosition = headerBranding.getBoundingClientRect().bottom + document.documentElement.scrollTop;
  }

  // Reset the nav position after all header images like the logo are done
  // loading in case they cause the header height to change.
  let loadedCounter = 0;
  const loadedImagesCounter = () => {
    loadedCounter += 1;
    if (loadedCounter === headerImages.length) {
      setNavPosition();
    }
  };

  headerImages.forEach(element => {
    if (element.complete) {
      loadedImagesCounter();
    }
    else {
      element.addEventListener('load', loadedImagesCounter);
    }
  });

  // Toggle the .is-fixed class when scrolling.
  window.addEventListener('scroll', function() {
    if (this.pageYOffset > navPosition) {
      fixed.classList.add('is-fixed');
      navbar.classList.add('is-fixed');
    }
    else {
      fixed.classList.remove('is-fixed');
      navbar.classList.remove('is-fixed');
    }
  });

  // Set the css variable of the nav bar height. This helps for when a menu
  // overflows to the second level.
  const setPageOffset = () => {
    let navbarHeight = navbar.offsetHeight;
    // Use the top link height if this is the mega menu.
    if (mega) {
      navbarHeight = topLink.offsetHeight;
    }
    root.style.setProperty('--fixed-page-offset', `${navbarHeight}px`);
  }

  // Watch for changes to the browser size.
  const resizeObserver = new ResizeObserver(entries => {
    entries.forEach(() => {
      setPageOffset();
      setNavPosition();
    })
  });

  const mobileSwitch = (mql) => {
    if (mql.matches) {
      resizeObserver.observe(navbar);
    } else {
      resizeObserver.unobserve(navbar);
    }
  };

  // Only watch for browser size changes in Desktop.
  mqMediumUp.addListener(mobileSwitch);
  // On Load.
  mobileSwitch(mqMediumUp);

}
