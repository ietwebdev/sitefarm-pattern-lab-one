// Set a --page-width css variable for the page without scrollbars.
export default function pageWidth() {
  const root = document.documentElement;
  const item = root.style.getPropertyValue('--page-width');

  // Exit if the css variable is already set.
  if (item !== '') {
    return;
  }

  function setPageWidth() {
    root.style.setProperty('--page-width', `${root.clientWidth}px`);
  }

  // Initialize on page load.
  setPageWidth();

  // Watch page resizes.
  window.addEventListener('resize', setPageWidth);
}
