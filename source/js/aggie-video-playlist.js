export default function aggieVideoPlaylist() {

  const iframes = document.querySelectorAll('iframe');

  const uiVert = 'uiconf_id/47302283';
  const uiHorz = 'uiconf_id/24976561';

  iframes.forEach(element => {
    const src = element.getAttribute('src') || element.getAttribute('data-src');

    // If this video is not using the modern overlay we need to swap ui confs.
    if (src && src.includes('kaltura.com') && (src.includes(uiVert) || src.includes(uiHorz))) {
      const sizeCheck = checkSize(element);

      // Run on page load.
      sizeCheck();

      // Run on resize.
      const resizeObserver = new ResizeObserver(sizeCheck);
      resizeObserver.observe(element);
    }

    // if the iframe has no width and height attributes we need to set them.
    const noDimensions = !element.getAttribute('width') && !element.getAttribute('height');
    if (src && src.includes('kaltura.com') && src.includes('playlist') && noDimensions) {
      element.setAttribute('width', '640');
      element.setAttribute('height', '480');
      element.classList.add('responsive-iframe', 'responsive-iframe--playlist');
    }
  });

  function checkSize (element) {
    return () => {
      const width = element.offsetWidth;
      // Exit if the element is not visible.
      if (width < 1) {
        return
      }

      const originalSrc = element.getAttribute('src');
      let newSrc;
      let smallScreen = false;

      // Playlist to the right of video: uiconf_id 24976561
      // Playlist underneath video: uiconf_id 47302283
      if (width < 700) {
        smallScreen = true;
        newSrc = originalSrc.replace(uiHorz, uiVert);
      }
      else {
        newSrc = originalSrc.replace(uiVert, uiHorz);
      }

      if (originalSrc !== newSrc) {
        element.setAttribute('src', newSrc);
        element.classList.toggle('responsive-iframe--playlist-vertical', smallScreen);
      }
    };
  }

}