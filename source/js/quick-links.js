import { toggle } from 'slide-element';
import { toggleAriaExpanded } from './lib/accessiblity-functions.js';

export default function quickLinks() {

  const menu = document.querySelector('.quick-links__menu');

  // Exit if there are no quicklinks.
  if (!menu) {
    return;
  }

  const menuTitle = document.querySelector('.quick-links__title');
  const menuToggles = document.querySelectorAll('.quick-links__title .submenu-toggle');
  const focusable = menu.querySelectorAll('button, [href], [tabindex]:not([tabindex="-1"])');
  const firstFocusable = focusable[0];
  const submenuOpenClass = 'submenu-toggle--open';
  const mqMediumUp = window.matchMedia('(min-width: 992px)');
  let open = false;

  // Hide the menu tray.
  const hideMenu = () => {
    open = false;
    menu.removeAttribute('style');
    menu.style.display = 'none';
    menuToggles.forEach(menuToggle => {
      menuToggle.classList.remove(submenuOpenClass);
    })
    menuTitle.setAttribute('aria-expanded', 'false');
  }

  // Toggle the submenu on click.
  menuTitle.addEventListener('click', function (event) {
    // Exit early if a previous animation is still in process.
    if (menu.dataset.sliding === 'true') {
      return;
    }

    open = !open;
    menuToggles.forEach(menuToggle => {
      menuToggle.classList.toggle(submenuOpenClass);
    })
    toggleAriaExpanded(this);
    toggle(menu);

    // If there is no clientX then this was likely a keyboard press.
    if (open && event.clientX === 0) {
      firstFocusable.focus();
    }
  })

  // Hide the off-canvas menu when leaving focus.
  menu.addEventListener('focusout', () => {
    // Wait to ensure the newly focused element is outside the off-canvas tray.
    setTimeout(function () {
      // Close the off-canvas menu if the previous focus was in the menu and
      // The menu toggle is not being clicked again.
      if (document.activeElement === document.body ||
        document.activeElement === null ||
        document.activeElement.closest('.quick-links__menu') !== null
      ) {
        return;
      }
      hideMenu();
    }, 150);
  })

  // Hide the submenu when clicking off of it.
  document.body.addEventListener('click', event => {
    if (open && !event.target.closest('.quick-links')) {
      if (mqMediumUp.matches) {
        hideMenu();
      }
    }
  });

  // Hide the menu when the screen size changes.
  window.addEventListener('resize', hideMenu);

  // Hide the menu initially.
  menu.style.display = 'none';
  menu.classList.remove('u-hidden--visually');

}
