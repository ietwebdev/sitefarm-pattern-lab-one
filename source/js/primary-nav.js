import $ from 'jquery';
import submenuToggle, { removeToggleStyles } from './lib/submenu-toggle.js';
import './lib/jquery-hoverintent.js';

window.jQuery = window.jQuery || $;

export default async function primaryNav() {

  let superfishLoaded;
  // If dynamic imports are not supported by the browser then it should
  // gracefully fall back to the pure css dropdown.
  try {
    await import('superfish');
    superfishLoaded = true;
  } catch (e) {
    superfishLoaded = false;
  }

  const $menu = $('.primary-nav');
  const $menuMega = $('.primary-nav--mega');
  const $menuSuperfish = $('.primary-nav--superfish > .menu');
  const $menuSub = $('.primary-nav .menu .menu');
  const $menuSubLower = $('.primary-nav .menu .menu .menu');
  const $menuLink = $('.primary-nav a');
  const bpMediumUp = '(min-width: 992px)';
  const $submenuToggle = $('.primary-nav .submenu-toggle');
  const fixedHeader = document.querySelector('.l-header--fixed');
  const body = document.querySelector('body');
  let hoverTimer = false;
  const hoverDelay = 300;
  const maxDepth = 2; // How deep Superfish dropdowns should go

  const megaDropDown = () => {
    $menuMega.hoverIntent(() => {
      clearTimeout(hoverTimer);
      $menuMega.addClass('is-hover');
    }, () => {
      hoverTimer = setTimeout(() => {
        $menuMega.removeClass('is-hover');
      }, hoverDelay);
    });

    // Add focus events for accessibility
    $menuLink.on('focus', () => {
      clearTimeout(hoverTimer);
      $menuMega.addClass('is-hover');
    });
    $menuLink.on('focusout', () => {
      hoverTimer = setTimeout(() => {
        $menuMega.removeClass('is-hover');
      }, hoverDelay);
    });
  };

  const menuSwitch = (mql, legacy) => {
    removeToggleStyles($menuSub.get());

    // Desktop
    if (mql.matches || legacy) {
      // Initialize the Mega Menu
      if ($menuMega.length) {
        // Show submenus
        $menuSub.show();
        // Hide Lower submenus
        $menuSubLower.removeAttr('style').hide();

        // Enable the megamenu dropdown
        megaDropDown();
      }

      // Initialize the Superfish dropdowns
      if ($menuSuperfish.length && superfishLoaded) {
        $menuSuperfish.superfish({
          hoverClass: 'sf--hover',
          delay: hoverDelay,
          cssArrows: false,
          onBeforeShow() {
            const depth = this.parents('.menu').length;
            // If the maximum depth has already been reached, remove the
            // classes signifying there are children
            if (depth === maxDepth) {
              $(this).find('a').removeClass('sf-with-ul');
            }

            // Don't show submenus lower than the maximum depth
            if (depth > maxDepth) {
              $(this).addClass('u-hidden--visually');
            }
          }
        });
      }

    }

    // Mobile
    else {
      // Disable the Mega Menu
      if ($menuMega.length) {
        // Remove hover bind
        $menu.off('mouseenter mouseleave');
      }

      // Disable the Superfish dropdowns
      if ($menuSuperfish.length && superfishLoaded) {
        $menuSuperfish.superfish('destroy');
        $menuSuperfish.find('.u-hidden--visually').removeClass('u-hidden--visually');
      }

      // Hide submenus
      $menuSub.removeAttr('style').hide();
      // Remove the mobile submenu toggle class
      $submenuToggle.removeClass('submenu-toggle--open');
    }
  };

  // Watch for changes to the browser size
  // get MediaQueryList Interface
  const mql = window.matchMedia(bpMediumUp);

  mql.addListener(menuSwitch);
  // On Load
  menuSwitch(mql);

  // Toggle submenu when clicked
  submenuToggle($submenuToggle.get());

  // If this is using a Mega Menu, ensure that we inform the rest of the site.
  if ($menuMega.length) {
    if (fixedHeader) {
      fixedHeader.classList.add('has-mega')
    } else {
      body.classList.add('has-mega')
    }
  }
}
