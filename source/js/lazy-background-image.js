// Lazyload background images that have data-lazy-bg-url="url(https://url.com)"
export default function lazyBackgroundImage() {
  const lazyBackgrounds = document.querySelectorAll('[data-lazy-bg-url]');

  if (!('IntersectionObserver' in window)) {
    return;
  }

  const lazyBackgroundObserver = new IntersectionObserver(entries => {
    entries.forEach(entry => {
      if (entry.isIntersecting) {
        const element = entry.target;
        element.style.setProperty('--lazy-bg-url', element.dataset.lazyBgUrl);
        lazyBackgroundObserver.unobserve(element);
      }
    });
  });

  lazyBackgrounds.forEach(lazyBackground => {
    lazyBackgroundObserver.observe(lazyBackground);
  })

}
