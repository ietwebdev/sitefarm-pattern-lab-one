import { down, toggle, up } from 'slide-element';
import { setCollapseAccessibility, toggleAriaExpanded } from './lib/accessiblity-functions.js';

export default function listAccordion() {

  const buttons = document.querySelectorAll('.list--accordion > li:nth-child(odd)');
  const content = document.querySelectorAll('.list--accordion > li:nth-child(even)');
  const accordion = document.querySelectorAll('.list--accordion');

  // Create expand/collapse buttons
  accordion.forEach(item => {
    item.insertAdjacentHTML('beforebegin', '<div class="accordion-accessiblity"><button class="accordion-accessiblity__expand">Expand All</button> | <button class="accordion-accessiblity__collapse">Collapse All</button></div>');
  });

  const expandBtn = document.querySelectorAll('button.accordion-accessiblity__expand');
  const collapseBtn = document.querySelectorAll('button.accordion-accessiblity__collapse');

  setCollapseAccessibility('.list--accordion > li:nth-child(odd)', '.list--accordion > li:nth-child(even)', 'accordion');

  // Accessibility: hide content
  content.forEach(item => {
    item.style.display = 'none';
  });

  // Slide toggle on button click or pressing enter
  buttons.forEach(button => {
    button.addEventListener('click', listAccordionToggle);
    button.addEventListener('keypress', listAccordionToggle);
  });

  // Expand all on button click
  expandBtn.forEach(btn => {
    btn.addEventListener('click', listAccordionExpand);
    btn.addEventListener('keypress', listAccordionExpand);
  });

  // Expand all on button click
  collapseBtn.forEach(btn => {
    btn.addEventListener('click', listAccordionCollapse);
    btn.addEventListener('keypress', listAccordionCollapse);
  });

  function listAccordionToggle(event) {
    if (event.keyCode === 13 || event.type === 'click') {
      const panel = this.nextElementSibling;

      this.classList.toggle('active');
      toggleAriaExpanded(this);
      toggle(panel);
    }
  }

  function listAccordionExpand(event) {
    if (event.keyCode === 13 || event.type === 'click') {
      const list = this.parentElement.nextElementSibling.querySelectorAll(':scope > li');
      Array.from(list).forEach((item, i) => {
        if (i % 2 === 0) {
          // The button.
          item.classList.add('active');
          item.setAttribute('aria-expanded', 'true');
        }
        else {
          // The panel.
          // Exit early if already open.
          if (item.style.display === 'block') {
            return;
          }
          down(item);
        }
      });
    }
  }

  function listAccordionCollapse(event) {
    if (event.keyCode === 13 || event.type === 'click') {
      const list = this.parentElement.nextElementSibling.querySelectorAll(':scope > li');
      Array.from(list).forEach((item, i) => {
        if (i % 2 === 0) {
          // The button.
          item.classList.remove('active');
          item.setAttribute('aria-expanded', 'false');
        }
        else {
          // The panel.
          up(item);
        }
      });
    }
  }
}
