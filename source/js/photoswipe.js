import $ from 'jquery';
import PhotoSwipe from 'photoswipe';
import PhotoSwipeUI from 'photoswipe/dist/photoswipe-ui-default';

export default function photoSwipe() {

  const $galleryItem = $('.gallery a');
  const $dialog = $('.pswp')[0];
  const items = [];

  // Create an array of all the slides
  $galleryItem.each(function () {
    const $size = $(this).data('size').split('x');

    const item = {
      src: $(this).attr('href'),
      w: $size[0],
      h: $size[1],
      title: $(this).attr('title'),
      msrc: $(this).find('img').attr('src')
    };

    items.push(item);
  });

  // Open Photoswipe when clicking a thumbnail
  $galleryItem.on('click', function (event) {
    event.preventDefault();

    // First unbind the click event that PatternLab adds
    $(this).removeAttr('onclick');

    const index = $galleryItem.index(this);
    const thumbnail = $(this).find('img')[0];
    const pageYScroll = window.pageYOffset || document.documentElement.scrollTop;
    const rect = thumbnail.getBoundingClientRect();

    const options = {
      index,
      bgOpacity: 0.95,
      showHideOpacity: true,
      shareEl: false,
      getThumbBoundsFn() {
        return {x: rect.left, y: rect.top + pageYScroll, w: rect.width};
      }
    };

    // Initialize PhotoSwipe
    const lightBox = new PhotoSwipe($dialog, PhotoSwipeUI, items, options);
    lightBox.init();
  });

}
