import { down, toggle, up } from 'slide-element';
import { setCollapseAccessibility, toggleAriaExpanded } from './lib/accessiblity-functions.js';

export default function listFaq() {

  const buttons = document.querySelectorAll('.list--faq > li:nth-child(odd)');
  const content = document.querySelectorAll('.list--faq > li:nth-child(even)');
  const faq = document.querySelectorAll('.list--faq');

  // Create expand/collapse buttons
  faq.forEach(item => {
    item.insertAdjacentHTML('beforebegin', '<div class="accordion-accessiblity"><button class="accordion-accessiblity__expand">Expand All</button> | <button class="accordion-accessiblity__collapse">Collapse All</button></div>');
  });

  const expandBtn = document.querySelectorAll('button.accordion-accessiblity__expand');
  const collapseBtn = document.querySelectorAll('button.accordion-accessiblity__collapse');

  setCollapseAccessibility('.list--faq > li:nth-child(odd)', '.list--faq > li:nth-child(even)', 'faq');

  // Accessibility: hide content
  content.forEach(item => {
    item.style.display = 'none';
  });

  // Slide toggle on button click or pressing enter.
  buttons.forEach(button => {
    button.addEventListener('click', faqAccordionToggle);
    button.addEventListener('keypress', faqAccordionToggle);
  });

  // Expand all on button click
  expandBtn.forEach(btn => {
    btn.addEventListener('click', faqAccordionExpand);
    btn.addEventListener('keypress', faqAccordionExpand);
  });

  // Expand all on button click
  collapseBtn.forEach(btn => {
    btn.addEventListener('click', faqAccordionCollapse);
    btn.addEventListener('keypress', faqAccordionCollapse);
  });

  function faqAccordionToggle(event) {
    if (event.keyCode === 13 || event.type === 'click') {
      const panel = this.nextElementSibling;

      this.classList.toggle('active');
      toggleAriaExpanded(this);
      toggle(panel);
    }
  }

  function faqAccordionExpand(event) {
    if (event.keyCode === 13 || event.type === 'click') {
      const list = this.parentElement.nextElementSibling.querySelectorAll(':scope > li');
      Array.from(list).forEach((item, i) => {
        if (i % 2 === 0) {
          // The button.
          item.classList.add('active');
          item.setAttribute('aria-expanded', 'true');
        }
        else {
          // The panel.
          // Exit early if already open.
          if (item.style.display === 'block') {
            return;
          }
          down(item);
        }
      });
    }
  }

  function faqAccordionCollapse(event) {
    if (event.keyCode === 13 || event.type === 'click') {
      const list = this.parentElement.nextElementSibling.querySelectorAll(':scope > li');
      Array.from(list).forEach((item, i) => {
        if (i % 2 === 0) {
          // The button.
          item.classList.remove('active');
          item.setAttribute('aria-expanded', 'false');
        }
        else {
          // The panel.
          up(item);
        }
      });
    }
  }
}
