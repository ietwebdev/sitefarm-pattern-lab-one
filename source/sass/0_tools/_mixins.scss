@use "sass:color";
@use "sass:list";
@use "sass:math";
@use "breakpoint-sass" as *;
@use "../variables" as *;

//------------------------------------
//  $MIXINS
//------------------------------------

// Create a darker shade of a color.
@function shade($color, $percent) {
  @return color.mix(#000, $color, $percent);
}

// Create a lighter tint of a color.
@function tint($color, $percent) {
  @return color.mix(#fff, $color, $percent);
}

// Make list flush to the left side
@mixin list-flush() {
  margin: 0;
  // add padding so that bullet points still show up inline
  padding: 0 0 0 $spacer + ($spacer * .25);
}

// Reset lists to that they have no styling
@mixin reset-list() {
  @include list-flush();
  padding-left: 0;
  list-style: none;

  li {
    list-style: none;
  }
}

// Fluid Typography
// @link https://css-tricks.com/snippets/css/fluid-typography/
@function strip-unit($value) {
  @return math.div($value, $value * 0 + 1);
}

@mixin fluid-type($min-vw, $max-vw, $min-font-size, $max-font-size) {
  $unit1: math.unit($min-vw);
  $unit2: math.unit($max-vw);
  $unit3: math.unit($min-font-size);
  $unit4: math.unit($max-font-size);

  @if $unit1 == $unit2 and $unit1 == $unit3 and $unit1 == $unit4 {
    // Hack for Safari because it is the new IE. Prevent it from only resizing
    // on page reload.
    min-height: 0;

    font-size: $min-font-size;
    @media screen and (min-width: $min-vw) {
      font-size: calc(#{$min-font-size} + #{strip-unit($max-font-size - $min-font-size)} * ((100vw - #{$min-vw}) / #{strip-unit($max-vw - $min-vw)}));
    }
    @media screen and (min-width: $max-vw) {
      font-size: $max-font-size;
    }
  }
}

// Hide text for image replacement
@mixin hide-text() {
  overflow: hidden;
  text-indent: 110%;
  white-space: nowrap;
}

// Reset the Heading tags which will be used in a list of items
@mixin reset-list-titles() {
  margin-bottom: 0;
  font-size: 1rem;
  font-style: normal;
}

// Create a circle border around an element
// @param {string} $border-size [1px] - Border size of the circle
// @param {string} $border-color [$brand--gray] - Color used for the border
@mixin circle-border($border-size: 1px, $border-color: $brand--gray) {
  border: $border-size solid $border-color;
  border-radius: 50%;
}

// Create a circular elements
// @param {string} $width - Diameter of the circle
// @param {string} $color - Color of the circle
@mixin circle($width, $color: "") {
  width: $width;
  height: $width;
  border-radius: math.div($width, 2);
  background: #{$color};
}

// Transform scale smoothes rough borders in some browsers (aka Firefox)
@mixin force-anti-alias() {
  transform: scale(.9999);
}

// Base focus styles. This will set what kind of style will be applied when an
// element has focus.
// @param {string} $type [default] - default, form, or reset
@mixin set-focus($type: "default") {

  @if $type == "default" {
    color: $link-color-focus;
  }
  @else if $type == "form" {
    border-color: $brand--secondary;
    background-color: $input-bg-focus;
    outline: none;
  }
  @else if $type == "reset" {
    background-color: inherit;
    outline: none;
    text-decoration: none;
  }
}

// Remove default form input styles
@mixin remove-input-style() {
  height: auto;
  box-shadow: none;
}

// Hide Visually
@mixin hide-visually() {
  position: absolute;
  clip: rect(1px, 1px, 1px, 1px);
  overflow: hidden;
  width: 1px;
  height: 1px;
  padding: 0;
  border: 0;
}

// Show Visually
@mixin show-visually() {
  position: inherit;
  clip: auto;
  width: auto;
  height: auto;
}

// Responsive sprite
//
// The sprite image should be horizontal and each image in the sprite should be equally spaced.
// @param {string} $sprite-url - URL path to the background sprite image
// @param {list} $images - Array of all the individual icon/image names
// @param {string} $width [100%] - How wide a single icon/image from the sprite will be
// @param {string} $padding-bottom [100%] - Total height the icon/image will be
@mixin responsive-sprite($sprite-url, $images: (), $width: 100%, $padding-bottom: 100%) {

  $ratio: math.div(100%, list.length($images) - 1);
  display: block;
  width: $width;
  height: 0;
  padding-bottom: $padding-bottom;
  background-image: url($sprite-url);
  background-position: 0 0;
  background-repeat: no-repeat;
  background-size: 100% * list.length($images);

  @for $i from 1 through list.length($images) {
    $image: list.nth($images, $i);

    &--#{$image} {
      background-position: ($ratio * $i - 1) 0;
    }
  }
}

// Standard Sprite generator
//
// The sprite image should be horizontal and each image in the sprite should be equally spaced.
// @param {string} $sprite-url - URL path to the background sprite image
// @param {list} $images - Array of all the individual icon/image names
// @param {string} $width [50px] - How wide a single icon/image from the sprite will be
// @param {string} $height [50px] - Total height the icon/image will be
@mixin horizontal-sprite($sprite-url, $images: (), $width: 50px, $height: 50px) {
  display: block;
  width: $width;
  height: $height;
  background-image: url($sprite-url);
  background-position: 0 0;
  background-repeat: no-repeat;

  @for $i from 1 through list.length($images) {
    $image: list.nth($images, $i);

    &--#{$image} {
      background-position: -($width * ($i - 1)) 0;
    }
  }
}

// Vertical Sprite position modifier generator
//
// This will create a modifier class for each image in a vertical sprite
// The sprite image should be vertical and each image in the sprite should be equally spaced.
// @param {list} $images - Array of all the individual icon/image names
// @param {string} $size [50px] - Total height the icon/image will be
@mixin vertical-sprite-position($images: (), $size: 100px) {
  @for $i from 1 through list.length($images) {
    $image: list.nth($images, $i);

    &--#{$image} {
      background-position: 0 (-($size * ($i - 1)));
    }
  }
}

// Add padding in so that content is padded to the width of the current container
@mixin container-content-padding() {
  @include breakpoint($bp-medium--up) {
    padding-right: $container-space--md;
    padding-left: $container-space--md;
  }

  @include breakpoint($bp-wide--up) {
    padding-right: $container-space--lg;
    padding-left: $container-space--lg;
  }

  @include breakpoint($bp-huge) {
    padding-right: $container-space--xlg;
    padding-left: $container-space--xlg;
  }
}

// Font Generator - based on the Style Linking technique described at https://www.smashingmagazine.com/2013/02/setting-weights-and-styles-at-font-face-declaration/
@mixin font-face($font-family, $file-path, $font-weight, $font-style) {
  @font-face {
    font-display: swap;
    font-family: $font-family;
    font-style: $font-style;
    font-weight: $font-weight;
    src: url("#{$file-path}.woff2") format("woff2"),
      url("#{$file-path}.woff") format("woff"),
      url("#{$file-path}.ttf") format("truetype"),
      url("#{$file-path}.svg##{$font-family}") format("svg");
  }
}

// Primary Link Styling
@mixin link-style() {
  color: $link-color;
  text-decoration: underline;

  &:hover {
    color: $link-color-hover;
  }
}

// Secondary Link Styling
@mixin link-style-secondary() {
  color: var(--forced-contrast, #{$link-color--secondary});
  text-decoration: underline;

  &:hover {
    color: var(--forced-contrast, #{$link-color-hover--secondary});
    text-decoration: none;
  }
}

// Heading Link Style
@mixin link-style-heading($h-link-color: $brand--primary) {

  a {
    color: var(--forced-contrast, #{$h-link-color});
    text-decoration: underline;

    &:hover,
    &:focus {
      color: var(--forced-contrast, #{$h-link-color});
      text-decoration: none;
    }
  }
}

// Convert a hex value to comma-delimited rgb values
// @param {string} $hex [#000000] - Hexidecimal color value.
// @return {string} [0,0,0] - color value in RGB.
@function hex-to-rgb($hex) {
  @return color.channel($hex, "red", $space: rgb), color.channel($hex, "green", $space: rgb), color.channel($hex, "blue", $space: rgb);
}

@mixin category-brand-background($default-bg: $brand--primary-80) {
  background-color: var(--category-brand, #{$default-bg});
}

@mixin category-brand-background--lighten($default-bg-rgb: $brand--primary-80) {
  background-color: rgba(var(--category-brand-rgb, #{$default-bg-rgb}), .1);
}

@mixin category-brand-knockout($default-bg: $brand--primary-80) {
  @include category-brand-background($default-bg);
  color: var(--category-brand-contrast-color, #{$white});
}

@mixin has-icon($direction: left) {
  display: flex;
  align-items: center;

  @if ($direction == left) {
    &:before {
      margin-right: $spacer--small;
      font-family: $font-family--icons;
      font-weight: $font-weight--icons;
    }
  }
  @else {
    &:after {
      margin-left: $spacer--small;
      font-family: $font-family--icons;
      font-weight: $font-weight--icons;
    }
  }
}

@mixin primary-nav-angle($direction: left, $bg: transparent, $bg-hover: $menu-background-color-active, $size: $spacer) {
  @if ($direction == left) {
    margin-left: $size;

    &:before {
      width: $size;
      height: $menu-height;
      margin-right: math.div($size, 2);
      margin-left: -$size;
      background-color: $bg;
      // Send clip off the sides so that subpixel render problems do not occur.
      clip-path: polygon(93% 0, 110% 0, 110% 102%, 0% 102%);
      content: "";
    }

    &:focus:before,
    &:hover:before {
      background-color: $bg-hover;
    }
  }
  @else {
    &:after {
      z-index: 1;
      width: $spacer;
      height: $menu-height;
      margin-right: -$size;
      margin-left: $spacer--small;
      background-color: $bg;
      // Send clip off the sides so that subpixel render problems do not occur.
      clip-path: polygon(-2px -2px, 100% -2px, 7% 102%, -2px 100%);
      content: "";
    }

    &:focus:after,
    &:hover:after {
      background-color: $bg-hover;
    }
  }
}

// Default settings for grid layouts.
@mixin l-default-settings() {
  --l-gap: #{$grid-gap};
  display: grid;
  // Allow components to override the gap without affecting the l-gap property.
  grid-column-gap: var(--l-gap-override, var(--l-gap));
  // A 12 column grid is explicitly used by default because doing layouts like
  // thirds or "1fr 2fr" actually creates slightly offset alignment of columns.
  grid-template-columns: repeat(12, 1fr);
  grid-template-rows: max-content 1fr;
}

// Force a top margin on siblings of layouts when stacked vertically in mobile.
@mixin l-stack-flow($spacer: false) {
  @include breakpoint($bp-medium--down) {
    > :where(* + *) {
      @if $spacer {
        margin-top: $spacer;
      } @else {
        margin-top: $panel-spacer;
      }
    }

    // Remove any bottom margin on the last grandchild item in the layout.
    :where(* > * > :last-child) {
      margin-bottom: 0;
    }
  }
}

@mixin reduced-title() {
  font-size: var(--reduced-title-font-size);
}

@mixin vertical-center() {
  position: relative;
  top: 50%;
  transform: translateY(-50%);
}

@mixin clearfix() {
  &:after {
    display: table;
    clear: both;
    content: "";
  }
}

@mixin button-as-text-link() {
  border: 0;
  background: transparent;
  color: var(--category-brand-contrast-color, var(--forced-contrast, #{$link-color}));
  text-decoration: underline;

  &:hover,
  &:focus {
    color: var(--category-brand-contrast-color, var(--forced-contrast, #{$link-color-hover}));
    text-decoration: none;
  }

  .category-brand__background & {
    color: var(--category-brand-contrast-color, inherit);
  }
}
