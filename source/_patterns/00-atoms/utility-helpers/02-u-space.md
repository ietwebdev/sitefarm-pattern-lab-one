---
title: Space

state: complete
---

Utility spacing classes add either margin or paddings to an element.

# Spacing Types and Direction

Spacing type and direction are abbreviated so that `margin-left` is `ml`.

## Margin Direction examples
```
.u-space-mx = margin-left and margin-right are set
.u-space-my = margin-top and margin-bottom are set
.u-space-mt = margin-top
.u-space-mr = margin-right
.u-space-mb = margin-bottom
.u-space-ml = margin-left
```

## Padding Direction examples
```
.u-space-px = padding-left and padding-right are set
.u-space-py = padding-top and padding-bottom are set
.u-space-pt = padding-top
.u-space-pr = padding-right
.u-space-pb = padding-bottom
.u-space-pl = padding-left
```

# Size Modifiers

Each direction can be modified by adding a suffix like `--large`.

## Margin Size examples
```
.u-space-mb--small = margin-bottom at half the normal spacing size.
.u-space-mb--medium = margin-bottom at one and a half the normal spacing size.
.u-space-mb--large = margin-bottom at double the normal spacing size.
.u-space-mb--flush = margin-bottom set to 0 so there is no spacing.
.u-space-m--flush = All margin is removed.
```

## Padding Size examples
```
.u-space-pb--small = padding-bottom at half the normal spacing size.
.u-space-pb--large = padding-bottom at double the normal spacing size.
.u-space-pb--flush = padding-bottom set to 0 so there is no spacing.
.u-space-p--flush = All padding is removed.
```

## Remove default vertical margin collapsing

Be aware that this can cause problems due to changing the display type.
```
.u-space--no-margin-collapse
```

# All Spacing Classes
```
.u-space-mx {
  margin-left: 1rem !important;
  margin-right: 1rem !important;
}

.u-space-mx--small {
  margin-left: 0.5rem !important;
  margin-right: 0.5rem !important;
}

.u-space-mx--medium {
  margin-left: 1.5rem !important;
  margin-right: 1.5rem !important;
}

.u-space-mx--large {
  margin-left: 2rem !important;
  margin-right: 2rem !important;
}

.u-space-mx--huge {
  margin-left: 4rem !important;
  margin-right: 4rem !important;
}

.u-space-mx--vast {
  margin-left: 8rem !important;
  margin-right: 8rem !important;
}

.u-space-mx--flush {
  margin-left: 0 !important;
  margin-right: 0 !important;
}

.u-space-my {
  margin-top: 1rem !important;
  margin-bottom: 1rem !important;
}

.u-space-my--small {
  margin-top: 0.5rem !important;
  margin-bottom: 0.5rem !important;
}

.u-space-my--medium {
  margin-top: 1.5rem !important;
  margin-bottom: 1.5rem !important;
}

.u-space-my--large {
  margin-top: 2rem !important;
  margin-bottom: 2rem !important;
}

.u-space-my--huge {
  margin-top: 4rem !important;
  margin-bottom: 4rem !important;
}

.u-space-my--vast {
  margin-top: 8rem !important;
  margin-bottom: 8rem !important;
}

.u-space-my--flush {
  margin-top: 0 !important;
  margin-bottom: 0 !important;
}

.u-space-mt {
  margin-top: 1rem !important;
}

.u-space-mt--small {
  margin-top: 0.5rem !important;
}

.u-space-mt--medium {
  margin-top: 1.5rem !important;
}

.u-space-mt--large {
  margin-top: 2rem !important;
}

.u-space-mt--huge {
  margin-top: 4rem !important;
}

.u-space-mt--vast {
  margin-top: 8rem !important;
}

.u-space-mt--flush {
  margin-top: 0 !important;
}

.u-space-mr {
  margin-right: 1rem !important;
}

.u-space-mr--small {
  margin-right: 0.5rem !important;
}

.u-space-mr--medium {
  margin-right: 1.5rem !important;
}

.u-space-mr--large {
  margin-right: 2rem !important;
}

.u-space-mr--huge {
  margin-right: 4rem !important;
}

.u-space-mr--vast {
  margin-right: 8rem !important;
}

.u-space-mr--flush {
  margin-right: 0 !important;
}

.u-space-mb {
  margin-bottom: 1rem !important;
}

.u-space-mb--small {
  margin-bottom: 0.5rem !important;
}

.u-space-mb--medium {
  margin-bottom: 1.5rem !important;
}

.u-space-mb--large {
  margin-bottom: 2rem !important;
}

.u-space-mb--huge {
  margin-bottom: 4rem !important;
}

.u-space-mb--vast {
  margin-bottom: 8rem !important;
}

.u-space-mb--flush {
  margin-bottom: 0 !important;
}

.u-space-ml {
  margin-left: 1rem !important;
}

.u-space-ml--small {
  margin-left: 0.5rem !important;
}

.u-space-ml--medium {
  margin-left: 1.5rem !important;
}

.u-space-ml--large {
  margin-left: 2rem !important;
}

.u-space-ml--huge {
  margin-left: 4rem !important;
}

.u-space-ml--vast {
  margin-left: 8rem !important;
}

.u-space-ml--flush {
  margin-left: 0 !important;
}

.u-space-m--flush {
  margin: 0 !important;
}

.u-space-px {
  padding-left: 1rem !important;
  padding-right: 1rem !important;
}

.u-space-px--small {
  padding-left: 0.5rem !important;
  padding-right: 0.5rem !important;
}

.u-space-px--medium {
  padding-left: 1.5rem !important;
  padding-right: 1.5rem !important;
}

.u-space-px--large {
  padding-left: 2rem !important;
  padding-right: 2rem !important;
}

.u-space-px--huge {
  padding-left: 4rem !important;
  padding-right: 4rem !important;
}

.u-space-px--vast {
  padding-left: 8rem !important;
  padding-right: 8rem !important;
}

.u-space-px--flush {
  padding-left: 0 !important;
  padding-right: 0 !important;
}

.u-space-py {
  padding-top: 1rem !important;
  padding-bottom: 1rem !important;
}

.u-space-py--small {
  padding-top: 0.5rem !important;
  padding-bottom: 0.5rem !important;
}

.u-space-py--medium {
  padding-top: 1.5rem !important;
  padding-bottom: 1.5rem !important;
}

.u-space-py--large {
  padding-top: 2rem !important;
  padding-bottom: 2rem !important;
}

.u-space-py--huge {
  padding-top: 4rem !important;
  padding-bottom: 4rem !important;
}

.u-space-py--vast {
  padding-top: 8rem !important;
  padding-bottom: 8rem !important;
}

.u-space-py--flush {
  padding-top: 0 !important;
  padding-bottom: 0 !important;
}

.u-space-pt {
  padding-top: 1rem !important;
}

.u-space-pt--small {
  padding-top: 0.5rem !important;
}

.u-space-pt--medium {
  padding-top: 1.5rem !important;
}

.u-space-pt--large {
  padding-top: 2rem !important;
}

.u-space-pt--huge {
  padding-top: 4rem !important;
}

.u-space-pt--vast {
  padding-top: 8rem !important;
}

.u-space-pt--flush {
  padding-top: 0 !important;
}

.u-space-pr {
  padding-right: 1rem !important;
}

.u-space-pr--small {
  padding-right: 0.5rem !important;
}

.u-space-pr--medium {
  padding-right: 1.5rem !important;
}

.u-space-pr--large {
  padding-right: 2rem !important;
}

.u-space-pr--huge {
  padding-right: 4rem !important;
}

.u-space-pr--vast {
  padding-right: 8rem !important;
}

.u-space-pr--flush {
  padding-right: 0 !important;
}

.u-space-pb {
  padding-bottom: 1rem !important;
}

.u-space-pb--small {
  padding-bottom: 0.5rem !important;
}

.u-space-pb--medium {
  padding-bottom: 1.5rem !important;
}

.u-space-pb--large {
  padding-bottom: 2rem !important;
}

.u-space-pb--huge {
  padding-bottom: 4rem !important;
}

.u-space-pb--vast {
  padding-bottom: 8rem !important;
}

.u-space-pb--flush {
  padding-bottom: 0 !important;
}

.u-space-pl {
  padding-left: 1rem !important;
}

.u-space-pl--small {
  padding-left: 0.5rem !important;
}

.u-space-pl--medium {
  padding-left: 1.5rem !important;
}

.u-space-pl--large {
  padding-left: 2rem !important;
}

.u-space-pl--huge {
  padding-left: 4rem !important;
}

.u-space-pl--vast {
  padding-left: 8rem !important;
}

.u-space-pl--flush {
  padding-left: 0 !important;
}

.u-space-p--flush {
  margin: 0 !important;
}
```

# Deprecated classes

The older `u-space-bottom` style classes will no longer be supported.

