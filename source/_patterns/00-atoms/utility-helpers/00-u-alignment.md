---
title: Alignment

state: complete
---

Alignments for elements like images which will be floated.

These alignments will only happen if there is at least `175px` for content next
to it. The floats use `!important` to force the alignment, but the margins can
be overridden.

## Alignment classes
```
.u-align--left
.u-align--center
.u-align--right
```

## Responsive media alignment

Removes alignment of img, video, and iframe tags if not enough space for text
next to the element.

```
.u-align--stack
```

## Clearing classes to break alignment.
```
.u-clearfix
.u-clear
```

`.u-clearfix` is a standard Clearfix class [https://learnlayout.com/clearfix.html](https://learnlayout.com/clearfix.html)

`.u-clear` simply clears all floats.
