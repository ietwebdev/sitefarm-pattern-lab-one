---
title: Visibility

state: complete
---

Utility classes to control visibility state. `!important` is used to force
the visibility state.

`.u-block` - Display as a block element.

`.u-inline` - Display as an inline element.

`.u-inline-block` - Display as an inline-block element.

`.u-hidden` - Hide the element.

`.u-hidden--visually` - Visually hide the element but make it visible for screen
readers.

`.u-shown--visually` - Show an element previously hidden with
`.u-hidden--visually`.

`.u-hide-text` - Hide the text in an element so only the background will remain.
