---
title: Widths

state: complete
---

Utility classes for widths. These classes only affect elements when the screen
size is above `768px` and will have the `!important` tag to force the width.
