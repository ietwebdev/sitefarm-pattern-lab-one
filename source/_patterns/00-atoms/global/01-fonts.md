---
title: Fonts
---

Proxima Nova, the primary official san-serif font for UC Davis, is available by default to ucdavis.edu sites managed in SiteFarm, the Drupal-based campus CMS.
