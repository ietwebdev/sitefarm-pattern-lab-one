---
title: Brand Colors
---

These are our primary university colors. They should be used emphatically in our communications. They do not necessarily have to be the dominant colors in every piece, but they should be used in such a way that they stand out. They can be dominant in a layout or they can be repeated more consistently than secondary colors when they are not dominant.
