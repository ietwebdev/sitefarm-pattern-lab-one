{
  "title": "Newborn Horses Give Clues to Autism",
  "subTitle": "Read How UC Davis Researchers Believe Solutions for Newborn Horses May be Linked to Helping Newborn Humans",
  "bodyClass": "article",
  "breadcrumbs": [
    {
      "url": "../04-pages-00-homepage/04-pages-00-homepage.html",
      "linkTitle": "Home"
    },
    {
      "url": "",
      "linkTitle": "News"
    },
    {
      "url": "",
      "linkTitle": "Newborn Horses Give Clues to Autism"
    }
  ],
  "author": {
    "firstName": "Pat",
    "lastName": "Bailey"
  },
  "singleCategory": [
    {
      "tagList": [
        {
          "linkTitle": "Agriculture"
        }
      ]
    }
  ],
  "multiCategory": [
    {
      "tagList": [
        {
          "linkTitle": "Veterinary Medicine"
        },
        {
          "linkTitle": "Research"
        },
        {
          "linkTitle": "Health"
        }
      ]
    }
  ],
  "pageCopy": {
    "section01": {
      "heading": "",
      "text": "<p>Just a few hours after its birth, the long-legged brown foal stands in its stall, appearing on first glance to be sound, sturdy and healthy. But something is very wrong with this newborn horse.</p><p>The foal seems detached, stumbles towards people and doesn’t seem to recognize its mother or have any interest in nursing. It even tries to climb into the corner feeder.</p><p>The bizarre symptoms are characteristic of a syndrome that has puzzled horse owners and veterinarians for a century. But recently, UC Davis researchers have discovered a surprising clue to the syndrome and intriguing similarities to childhood autism in humans.</p>"
    },
    "section02": {
      "heading": "Resembles children with autism",
      "text": "<p>“The behavioral abnormalities in these foals seem to resemble some of the symptoms in children with autism,” said John Madigan, a UC Davis veterinary professor and an expert in equine neonatal health.</p> <p>“There are thousands of potential causes for autism, but the one thing that all autistic children have in common is that they are detached,” said Isaac Pessah, a professor of molecular biosciences at the UC Davis School of Veterinary Medicine and a faculty member of the UC Davis MIND Institute, who investigates environmental factors that may play a role in the development of autism in children.</p> <p>Pessah, Madigan and other researchers in veterinary and human medicine recently formed a joint research group and secured funding to investigate whether abnormal levels of neurosteroids — a group of chemicals that modulate perception — may play a role in both disorders.</p> <p>They hope their efforts will help prevent and treat the disorder in foals and advance the search for the causes of autism, which affects more than 3 million individuals in the United States.</p>"
    },
    "section03": {
      "heading": "Maladjusted foal syndrome",
      "text": "<p>In newborn foals, the disorder known as neonatal maladjustment syndrome or dummy foal syndrome occurs in only 3 to 5 percent of live births. But when it does appear, it is, said one Thoroughbred horse breeder, “a nightmare.”</p> <p>With continuous treatment, including around-the-clock bottle or tube feeding plus intensive care in a veterinary clinic, 80 percent of the foals recover. But that level of care — required for up to a week or 10 days — is grueling and costly.</p> <p>For years, the syndrome has been attributed to hypoxia — insufficient oxygen during the birthing process. Typically, when a foal’s brain is deprived of oxygen, the resulting effects include mental deficits, abnormal behavior, blindness and even seizures. And, as in human babies, much of the damage is serious and permanent.</p>"
    },
    "section04": {
      "heading": "But is oxygen deprivation the culprit?",
      "text": "<p>Oddly, however, most foals with neonatal maladjustment syndrome survive the ordeal and have no lingering health problems. This raised the question of whether hypoxia was the culprit in the syndrome, and Madigan and UC Davis veterinary neurologist Monica Aleman began sleuthing around for other potential causes.</p> <p>One of their prime suspects was a group of naturally occurring neurosteroids, which are key to sustaining pregnancies in horses, especially in keeping the foal “quiet” before birth.</p>"
    },
    "section05": {
      "heading": "No galloping in the womb",
      "text": "<p>“Foals don’t gallop in utero,” Madigan is fond of saying, pointing out the dangers to the mare if a four-legged, hoofed fetus were to suddenly become active in the womb. The prenatal calm is made possible, he explained, by neurosteroids that act as sedatives for the unborn foal.</p> <p>However, immediately after birth, the infant horse must make an equally important transition to consciousness. In nature, a baby horse would be easy prey for many natural enemies, so the foal must be ready to run just a few hours after it is born.</p>"
    },
    "section06": {
      "heading": "Biochemical ‘on switch’",
      "text": "<p>In short, somewhere between the time a foal enters the birth canal and the moment it emerges from the womb, a biochemical “on switch” must be flicked that enables the foal to recognize the mare, nurse, and become mobile. Madigan and Aleman suspect that the physical pressure of the birthing process may be that important signal.</p> <p>“We believe that the pressure of the birth canal during the second stage of labor, which is supposed to last 20 to 40 minutes, is an important signal that tells the foal to quit producing the sedative neurosteroids and ‘wake up,’ ” Madigan said.</p>"
    }
  },
  "img": {
    "landscape4x3": {
      "src": "../../images/sample/article/swaddled_baby.jpg",
      "alt": "Swaddled baby"
    },
    "landscape16x9": {
      "src": "../../images/sample/article/foal.jpg",
      "alt": "Foal"
    },
    "profileImage": {
      "src": "../../images/sample/person/person-01-thumbnail.jpg",
      "alt": "User image placeholder."
    }
  },
  "peopleListShort": [
    {
      "img": {
        "thumbnail": {
          "src": "../../images/sample/person/person-01-thumbnail.jpg",
          "alt": "Janeka Elodia"
        }
      },
      "headline": {
        "medium": "Ms. Janeka Elodia",
        "short": "Web Developer"
      },
      "showDate": false
    },
    {
      "img": {
        "thumbnail": {
          "src": "../../images/sample/person/person-02-thumbnail.jpg",
          "alt": "Olivia Lizbeth"
        }
      },
      "headline": {
        "medium": "Olivia Lizbeth",
        "short": "Manager"
      },
      "showDate": false
    }
  ],
  "person": {
    "headline": "Ms. Janeka Elodia",
    "title": "Web Developer",
    "dept": "Information and Educational Technology (IET)",
    "summary": "<p>Janeka has been developing websites for a very long time. In fact, she created the internet! Because she made it all, she knows best how to create websites that people want to look at for hours and hours.</p>"
  },
  "wysiwygFeatureBlock": {
    "title": "Link between horses and humans?",
    "text": "<p>Read how UC Davis researchers believe solutions for newborn horses may be linked to helping newborn humans:</p> <ul> <li><a href=\"\">“Kangaroo care” for human babies</a></li> <li><a href=\"\">Is there a link to autism?</a></li> <li><a href=\"\">Pursuing further studies</a></li> </ul>"
  },
  "articleList": [
    {
      "img": {
        "thumbnail": {
          "src": "../../images/sample/article/almonds.jpg",
          "alt": "almonds"
        }
      },
      "headline": {
        "medium": "Almonds contribute little to carbon emissions, study finds"
      },
      "year": {
        "long": "2021"
      },
      "month": {
        "long": "July"
      },
      "day": {
        "short": "24"
      }
    },
    {
      "img": {
        "thumbnail": {
          "src": "../../images/sample/article/warbler.jpg",
          "alt": "warbler"
        }
      },
      "headline": {
        "medium": "Keep Tahoe blue? Less algae, not clarity, key factor for blueness"
      },
      "year": {
        "long": "2021",
        "short": "15"
      },
      "month": {
        "long": "July",
        "short": "Jul",
        "digit": "07"
      },
      "day": {
        "long": "23",
        "short": "23",
        "ordinal": "rd"
      }
    },
    {
      "img": {
        "thumbnail": {
          "src": "../../images/sample/article/vole.jpg",
          "alt": "vole"
        }
      },
      "headline": {
        "medium": "Lyme disease subverts immune system, prevents future protection"
      },
      "year": {
        "long": "2021",
        "short": "15"
      },
      "month": {
        "long": "July",
        "short": "Jul",
        "digit": "07"
      },
      "day": {
        "long": "02",
        "short": "2",
        "ordinal": "nd"
      }
    }
  ],
  "pullquotes": [
    {
      "author": {
        "firstName": "John",
        "lastName": "Madigan"
      },
      "excerpt": {
        "short": "There are reports of very sick newborn babies … making seemingly miraculous, spontaneous recoveries after being placed in the arms of a grieving parent for a last embrace."
      }
    }
  ],
  "categoryFilter": [
    {
      "brandColor": "category-brand--admin-blue",
      "isActiveLink": false,
      "text": "University News"
    },
    {
      "brandColor": "category-brand--double-decker",
      "text": "Society, Arts & Culture"
    },
    {
      "brandColor": "category-brand--cabernet",
      "text": "Food & Agriculture"
    },
    {
      "brandColor": "category-brand--quad",
      "text": "Environment"
    },
    {
      "brandColor": "category-brand--tahoe",
      "text": "Science & Technology"
    },
    {
      "brandColor": "category-brand--redbud",
      "text": "Student Life"
    },
    {
      "brandColor": "category-brand--poppy",
      "text": "Education"
    }
  ],
  "tagList": [
    {
      "linkTitle": "Autism"
    },
    {
      "linkTitle": "Veterinary Medicine"
    },
    {
      "linkTitle": "Equine Health"
    },
    {
      "linkTitle": "Research"
    },
    {
      "linkTitle": "Agriculture"
    },
    {
      "linkTitle": "One Health"
    },
    {
      "linkTitle": "Breakthrough"
    }
  ],
  "figcaption": "John Madigan loops a rope harness around a maladjusted foal.",
  "mediaResources": [
    {
      "mediaResource": "<a href='#'>Joe Proudman</a>, Joe Proudman is the multimedia specialist for UC Davis Strategic Communications."
    },
    {
      "mediaResource": "<a href='#'>Media Downloads</a>"
    }
  ],
  "quickSummaryItems": [
    {
      "quickSummaryItem": "Foals ‘wake up’ with gentle harness pressure"
    },
    {
      "quickSummaryItem": "Labor affects steroid levels"
    }
  ]
}
