# Javascript

It is assumed that a `main.js` file will be created here as the entry point for
Javascript on the site. When including this script in html, be sure to set it as
a `type="module"`

```
<script src="/dist/main.js" type="module"></script>
```

Javascript will be compiled to a `/dist/` directory.

Use native [ESM](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/import)
imports for scripts.

[Vite](https://vitejs.dev/) is used to compile all code.

## Pattern Lab Files

Javascript imported from Pattern Lab will be in the **/js/pattern_lab/** directory. Do not edit files here or they will be changed on the next sync.

## Validation

[ESLint](http://eslint.org/) is being used to validate javascript.

The npm script `npm run lint:js` can be used to easily validate all Javascript.

In addition, `npm run lint:js-fix` will automatically fix as many errors as it
can.
