module.exports = {
  themeSync: {
    src: '../pattern_lab/',
    sassSync: true,
    jsSync: true,
    imagesSync: true,
    fontSync: true,
  },
}
