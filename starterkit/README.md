# UCD Marketing Policies and Procedure
UCD Marketing Policies and Procedure: [http://marketingtoolbox.ucdavis.edu/resources/policy-procedure.html](http://marketingtoolbox.ucdavis.edu/resources/policy-procedure.html)

All usage of UCD branding must abide by Policy 310-65:
[http://manuals.ucdavis.edu/ppm/310/310-65.pdf](http://manuals.ucdavis.edu/ppm/310/310-65.pdf)


# Getting Started

## Prerequesites 

You'll need [node.js](http://nodejs.org).


## Install and setup

After cloning and changing into that directory, run this to install
dependencies:

```
$ npm install
```

You may have to run that again for updates; so it may be wise to save this: `$ npm install`. **If you have any problems; this is the first thing to run.**

To do an initial build of the site and start watching for changes while doing
local development run:

```
$ npm start
```

## Default Tasks

There are 3 main scripts you should be aware of. Just add `npm run` before
each task like `$ npm run build`.

1. **start** - Generate the theme assets and start watching for changes to live
   reload in the browser.
2. **build** - Generate the theme with all assets such as css and js.
3. **lint** - Validate CSS and JS by linting.

`$ npm start` is the one most often used and is a shorthand for `npm run start`


## Assets (CSS & JS)

Sass and Javascript will be automatically compiled into a `/dist/` directory.

### NPM Dependencies

To add either CSS or JS from third party packages to Pattern Lab, use one of
these methods:

* CSS: https://github.com/ucdavis/ucd-theme-tasks/blob/master/docs/css.md
* JS: https://github.com/ucdavis/ucd-theme-tasks/blob/master/docs/javascript.md


## UCD Theme Tasks

The [UCD Theme Tasks](https://www.npmjs.com/package/ucd-theme-tasks) node
package is a CLI tool. You likely will not need to use it directly, but it is
good to be aware that the NPM scripts are using it.

If you would like to use it directly then use the `npx` command to use the
locally installed version with your theme.

```
$ npx ucd-theme-tasks --help
```
